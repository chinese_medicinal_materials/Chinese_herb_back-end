
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 12:09:40 2018

@author: 16703
"""
import codecs

import matplotlib
from keras.models import load_model
import os
import util
import config
import numpy as np
import json
import cv2
from sys import argv
import matplotlib.pyplot as plt

def load_feature(img_path):
    img = util.cv_imread(img_path)
    crop = img / 255.
    crop = crop - 0.5
    crop = crop * 2.
    flipimg = crop
    crop = cv2.resize(flipimg, (config.INPUT_SIZE, config.INPUT_SIZE))
    return crop


def datasettest(paths):
    testimgs = []
    testnames = []
    for filename in os.listdir(paths):
        file = paths + '/' + filename
        testimgs.append(file)
        testnames.append(filename)
    return testimgs, testnames


def datatest_generator(img_paths, name, batch_size):
    num_sample = len(img_paths)
    print(num_sample)
    while True:
        for offset in range(0, num_sample, batch_size):
            batch_paths = img_paths[offset:offset + batch_size]
            batch_names = name[offset:offset + batch_size]
            batch_features = [load_feature(path) for path in batch_paths]
            batch_feature = np.array(batch_features)
            yield batch_feature, np.array(batch_names)

def as_num(x):
    y='{:.5f}'.format(x) # 5f表示保留5位小数点的float型
    return(y)

batch_size = 1
trian_img_paths, tnames = datasettest('D:/HerbTrain/AgriculturalDisease_trainA/images/')
# trian_img_paths,tnames = datasettest('./AgriculturalDisease_testA/images/')

train_data_gen = datatest_generator(trian_img_paths, tnames, batch_size)
model = load_model('D:/HerbTrain/my_model_resnet.h5')

xtest, tnames = next(train_data_gen)

out = model.predict(xtest)

#print(out)

'''
输出最大概率病害
'''
outmax=  np.argmax(out, 1)
out2 = int(outmax)
print(config.LABEL_NAMES[out2])

'''
for item in out1:
    print(str(config.LABEL_NAMES[i]),as_num(out1[i]))
    i = i+1
'''
out1 = out.ravel()


i = 0


labels = ["苹果健康", "苹果黑星病一般", "苹果黑星病严重", "苹果灰斑病", "苹果雪松锈病一般",
          "苹果雪松锈病严重", "樱桃健康", "樱桃白粉病一般", "樱桃白粉病严重", "玉米健康",
          "玉米灰斑病一般", "玉米灰斑病严重", "玉米锈病一般", "玉米锈病严重", "玉米叶斑病一般",
          "玉米叶斑病严重", "玉米花叶病毒病", "葡萄健康", "葡萄黑腐病一般", "葡萄黑腐病严重",
          "葡萄轮斑病一般", "葡萄轮斑病严重", "葡萄褐斑病一般", "葡萄褐斑病严重", "柑桔健康",
          "柑桔黄龙病一般", "柑桔黄龙病严重", "桃健康", "桃疮痂病一般", "桃疮痂病严重", "辣椒健康",
          "辣椒疮痂病一般", "辣椒疮痂病严重", "马铃薯健康", "马铃薯早疫病一般", "马铃薯早疫病严重",
          "马铃薯晚疫病一般", "马铃薯晚疫病严重", "草莓健康", "草莓叶枯病一般", "草莓叶枯病严重",
          "番茄健康", "番茄白粉病一般", "番茄白粉病严重", "番茄疮痂病一般", "番茄疮痂病严重", "番茄早疫病一般",
          "番茄早疫病严重", "番茄晚疫病菌一般", "番茄晚疫病菌严重", "番茄叶霉病一般", "番茄叶霉病严重",
          "番茄斑点病一般", "番茄斑点病严重", "番茄斑枯病一般", "番茄斑枯病严重", "番茄红蜘蛛损伤一般",
          "番茄红蜘蛛损伤严重", "番茄黄化曲叶病毒病一般"]
fracs = []
while 1:
    if i > 58:
        break;
    fracs.append(as_num(out1[i]))
    i = i+1

'''
count = 0
for item in fracs:
    print(fracs[count])
    count = count+1
'''
count = 0

#f = codecs.open("D:/HerbTrain/Analysis/probability.txt",'w','utf-8')
f = codecs.open("E:/IDEA_CODE/herbtest/src/main/resources/static/img/PiePhoto/probability.txt",'w','utf-8')

for item in fracs:
    f.write((fracs[count]+'\r\n'))
    count = count+1
f.close()
#print('img/PiePhoto/probability.txt')

# explode = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
#            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]  # 0.1 凸出这部分，
#
# matplotlib.rcParams['font.sans-serif']=['SimHei']  #使用指定的汉字字体类型（此处为黑体）

# plt.axes(aspect=1)  # set this , Figure is round, otherwise it is an ellipse
#此处的aspect=1表示正圆，取不同的值，表示的圆形状不同
# autopct ，show percet
# plt.pie(x=fracs, labels=labels, explode=explode, autopct='%3.1f %%',
#         shadow=True, labeldistance=1.1, startangle=90, pctdistance=0.6
#         )
'''
labeldistance，文本的位置离远点有多远，1.1指1.1倍半径的位置
autopct，圆里面的文本格式，%3.1f%%表示小数有三位，整数有一位的浮点数
shadow，饼是否有阴影
startangle，起始角度，0，表示从0开始逆时针转，为第一块。一般选择从90度开始比较好看
pctdistance，百分比的text离圆心的距离
patches, l_texts, p_texts，为了得到饼图的返回值，p_texts饼图内部文本的，l_texts饼图外label的文本
'''
#plt.title("病害分析结果")
#plt.show()
#plt.savefig('D:/HerbTrain/Analysis/result.png')
#plt.savefig('D:/HerbTrain/Analysis/result.png')
#print('img/PiePhoto/result.png')