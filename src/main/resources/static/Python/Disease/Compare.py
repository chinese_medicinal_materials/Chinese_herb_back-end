import os

import cv2
from PIL import Image
from PIL import ImageFile
import imagehash

def get_hash_dict(dir):
    hash_dict = {}
    image_quantity = 0
    for _, _, files in os.walk(dir):
        for i, fileName in enumerate(files):
            with open(dir + fileName, 'rb') as fp:
                hash_dict[dir + fileName] = imagehash.average_hash(Image.open(fp))
                image_quantity += 1

    return hash_dict, image_quantity


def compare_image_with_hash(image_file_name_1, image_file_name_2, max_dif=0):

    ImageFile.LOAD_TRUNCATED_IMAGES = True
    hash_1 = None
    hash_2 = None
    with open(image_file_name_1, 'rb') as fp:
        hash_1 = imagehash.average_hash(Image.open(fp))
    with open(image_file_name_2, 'rb') as fp:
        hash_2 = imagehash.average_hash(Image.open(fp))
    dif = hash_1 - hash_2
    if dif < 0:
        dif = -dif
    if dif <= max_dif:
        return True
    else:
        return False

def compare_image_with_hash_loop():

    ImageFile.LOAD_TRUNCATED_IMAGES = True


    path = "D:/HerbTrain/AgriculturalDisease_Save/images"  # 文件夹目录
    newpath = "D:/HerbTrain/AgriculturalDisease_trainA/images" #新上传图片
    files = os.listdir(path)  # 得到文件夹下的所有文件名称
    hash = []
    i = 0
    for filename in files:
        #print(filename) #just for test
        img = open(path + "/" + filename, 'rb')
        #print(img)
        hash.append(imagehash.average_hash(Image.open(img)))
        #print(hash[i])
        i = i + 1

    files_new = os.listdir(newpath)
    hash_new = []
    n = 0
    for filename_new in files_new:
        img_new = open(newpath + "/" + filename_new, 'rb')
        hash_new.append(imagehash.average_hash(Image.open(img_new)))
        n = n + 1

    #print(hash_new[0])

    for hashs in hash:
        if hash_new[0] == hashs:
            return 'same'
        else:
            return 'different'



'''
def main():

    print(compare_image_with_hash('D:\\HerbTrain\\AgriculturalDisease_Save\\images\\100.jpg',
                                  'D:\\HerbTrain\\AgriculturalDisease_Save\\images\\102.jpg', 6))

    
    
'''

if __name__ == '__main__':
    a = compare_image_with_hash_loop()
    print('asdads')
    print(a)