from bs4 import BeautifulSoup
import requests as req
import re
import os
import pymysql
import html2text as ht
import markdown
import datetime


class RootNewsConfig:
    url = ""
    root_url = ""
    NewsListConfig = ""
    ArticleHTMLTag = ""
    NewsType = ""
    css = "Markdown.css"

    def __init__(self, work_dir):
        with open(work_dir + self.css, 'r', encoding='UTF-8') as f:
            self.css = f.read()

    def GetSoup(self, url):
        resp = req.get(url)
        resp.encoding = resp.apparent_encoding
        html_source = resp.text
        return BeautifulSoup(html_source, 'lxml')

    def GetNewsList(self, soup):  # 返回一个列表，为整个网页包含新闻的列表
        aa = soup.find_all(name=self.NewsListConfig['listloc']['name'], attrs=self.NewsListConfig['listloc']['attrs'])
        if aa is None:
            return None
        NewsTags = aa[0].find_all(name=self.NewsListConfig['Newsloc']['name'], attrs=self.NewsListConfig['Newsloc']['attrs'])
        return NewsTags
    
    def GetNewsTitle(self, NewsTag):  # 每次调用只返回一个结果，没提取到则返回null
        title = NewsTag.get_text()
        if title is None:
            return 'NULL'
        else:
            return str(title)

    def GetNewsTime(self, NewsTag):  # 每次调用只返回一个结果，没提取到则返回null
        time = NewsTag.get('href')
        time = re.match('[\\d\\.]+', time).group()
        if time is None:
            return None
        time = re.match('\\d+\\.\\d+\\.\\d+', time+'.1').group()  # 将格式统一为xx.xx.xx
        if time is None:
            return 'NULL', None
        else:
            return str(time), datetime.datetime.strptime(time, '%Y.%m.%d')

    def GetNewsAbstract(self, NewsTag):
        url = self.GetNewsLink(NewsTag)
        soup = self.GetSoup(url)
        html = soup.find(name=self.ArticleHTMLTag['name'], attrs=self.ArticleHTMLTag['attrs'])
        if html is not None:
            Abstract = html.select('table tr table tr div')[1].get_text()[0:125] + '...'
        if Abstract is None:
            return 'NULL'
        return Abstract
    
    def GetNewsLink(self, NewsTag):  # 每次调用只返回一个结果，没提取到则返回null
        return str(self.root_url + NewsTag.get('href'))
    
    def GetNewsKeyWord(self, NewsTag):
        return 'NULL'

    def GetNewsImg(self, NewsTag):
        return 'NULL'

    def GetNewsArticle(self, NewsTag):
        url = self.GetNewsLink(NewsTag)
        soup = self.GetSoup(url)
        for ImgTag in soup.find_all(name='img'):
            ImgTag['src'] = self.root_url + ImgTag['src']
        html = soup.find(name=self.ArticleHTMLTag['name'], attrs=self.ArticleHTMLTag['attrs'])
        if html is not None:
            html = html.select('table tr table tr td')[6]
        text_maker = ht.HTML2Text()
        if html is not None:
            text = text_maker.handle(str(html))
        else:
            text = text_maker.handle(str(soup))
        NewHtml = markdown.markdown(text)
        return self.css + NewHtml, text  # 一个html一个markdown
