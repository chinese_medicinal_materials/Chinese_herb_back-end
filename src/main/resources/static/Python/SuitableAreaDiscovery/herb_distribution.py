import pandas as pd
import numpy as np
from sklearn.datasets.base import Bunch
from time import time
from sklearn import svm, metrics
from time import time
from math import ceil, floor
import os
from os import path
import sys


def create_herbs_bunch(herbs_name, herb_information):
    bunch = Bunch(name=''.join(herbs_name))
    herb_train_len = floor(len(herb_information) * 0.8)
    herb_train_information, herb_test_information = np.vsplit(herb_information, (herb_train_len,))
    bunch['pts_train'], bunch['cov_train'] = np.hsplit(herb_train_information, (3,))
    bunch['pts_test'], bunch['cov_test'] = np.hsplit(herb_test_information, (3,))
    return bunch


if __name__ == "__main__":
    herbName = sys.argv[1]
    print(herbName)
    # 记录程序开始时间
    t0 = time()
    # 从CSV文件之中读取验证集和训练集,其中使用的是根据主程序所在目录来执行
    d = path.dirname(__file__)
    herb_info_path = d + "/herb_distribute.csv"
    ecosystem_info_path = d + "/ecosystem.csv"
    herb_info = pd.read_csv(herb_info_path)
    ecosystem_info = pd.read_csv(ecosystem_info_path)
    # pandas读取结构转化为numpy的矩阵
    ecosystem_matrix = ecosystem_info.values
    # 矩阵划分为经纬度信息以及对应的环境变量的信息
    long_and_lat, ecosystem_var = np.hsplit(ecosystem_matrix, (6,))

    # 测试集处理，将其中的数据
    pre_herb_matrix = herb_info.values
    herb_name_index = (pre_herb_matrix[:, 0] == herbName)
    herb_matrix = pre_herb_matrix[herb_name_index]

    # 利用药材名和最后得到的所有训练集之中的药材，来创建指定药材的数据
    herb = create_herbs_bunch(herbName, herb_matrix)
    herb.cov_train = herb.cov_train.astype(float)

    # 标准化特征
    mean = herb.cov_train.mean(axis=0)
    std = herb.cov_train.std(axis=0)
    train_std = (herb.cov_train - mean) / std

    # 训练OneClassSVM模型
    # 其中nu是异常点的比例，kernel指的是rbf对应的是高斯核(一般用这个)
    clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma='auto')
    clf.fit(train_std)

    # 预测结果,pred本质上是验证集之中各个样本到超平面函数的距离
    pred = clf.decision_function((ecosystem_var - mean) / std)
    levels = np.linspace(pred.min(), pred.max(), 25)
    # 从中随机抽取一些点来计算最终的
    np.random.seed(10)
    background_point = np.random.randint(low=0, high=len(pred), size=10000)
    # pred_background即为取出的最终的点
    pred_background = pred[background_point]
    # 对测试集合利用该模型去进行估计
    pred_test = clf.decision_function((herb.cov_test - mean) / std)
    # 最后来计算得分，并且获得最终的AUC值
    scores = np.r_[pred_test, pred_background]
    y = np.r_[np.ones(pred_test.shape), np.zeros(pred_background.shape)]
    fpr, tpr, thresholds = metrics.roc_curve(y, scores)
    roc_auc = metrics.auc(fpr, tpr)
    print('AUC: %.3f' % (roc_auc))
    # 取出其中的一部分点认为是适生区的点
    arr = (long_and_lat[pred >= levels[23]]).tolist()
    # 将最终这些点的经纬度
    filePath = d +"/herb_distribute_list.txt"
    file = open(filePath, "w")
    last_item = [str(i) for i in arr[-1]]
    for line in arr:
        line = [str(i) for i in line]
        if line == last_item:
            file.write(','.join(line))
        else:
            file.write(','.join(line) + '\n')
    print(filePath)
    file.close()
    print("Totally running time:%.2fs" % (time() - t0))