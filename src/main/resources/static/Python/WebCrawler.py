# pip install pymysql
# pip install html2text
# pip install markdown
# 此程序用于配置需要爬取的网站的信息以便调用NewsCrawler.py进行爬取，
# 并对NewsCrawler.py爬取的结果进行处理

import io
import sys

sys.stderr = io.TextIOWrapper(sys.stderr.buffer, encoding='utf-8')
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')


import os
import pymysql
from NewsConfig import *

# 遍历所有网站的class
import inspect
tt = __import__('NewsConfig')
News_webs = inspect.getmembers(tt, inspect.isclass)

# 获取保存新闻文件的路径
HTML_loc = sys.argv[0].replace('Python/WebCrawler.py', 'News/')
# 工作路径(强制定义，并非实际工作路径，只是为了方便文件保存位置的确定)
work_dir = sys.argv[0].replace('Python/WebCrawler.py', 'Python/')

db = pymysql.connect(host="localhost", user="root", password="123456", database="herbsystem", charset="utf8")
cursor = db.cursor()

fbh = HTML_loc + 'HTML/'  # 网页保存位置
fbm = HTML_loc + 'MD/'  # 网页保存位置
if not os.path.exists(fbh):
    os.makedirs(fbh)
if not os.path.exists(fbm):
    os.makedirs(fbm)

WEB = None
for (name, _) in News_webs:
    if name == 'RootNewsConfig':
        continue
    exec('WEB = {}(work_dir)'.format(name))
    print(WEB.__doc__)
    # 查询数据库中当前网页已有最新的新闻日期
    latest_news = "select max(date) from `news` where refer_url like" + "\'" + WEB.root_url + "%\'" + " and type like " + "\'" + WEB.NewsType + "\'"
    cursor.execute(latest_news)
    Last_update = cursor.fetchone()[0]  # 最后一次更新的时间
    soup = WEB.GetSoup(WEB.url)
    NewsTags = WEB.GetNewsList(soup)
    if NewsTags is not None:
        for tag, ii in zip(NewsTags, range(len(NewsTags))):
            date, News_time = WEB.GetNewsTime(tag)
            if Last_update is not None:
                if News_time <= Last_update:
                    print(WEB.__doc__ + ":更新完成")
                    break
            title = WEB.GetNewsTitle(tag)
            Abstract = WEB.GetNewsAbstract(tag)
            refer_url = WEB.GetNewsLink(tag)
            NewsType = WEB.NewsType
            key_words = WEB.GetNewsKeyWord(tag)
            img = WEB.GetNewsImg(tag)
            print(str(ii)+':\t'+'%r'%title+'\t'+date)
            for ii in ['title', 'date', 'Abstract', 'refer_url', 'NewsType', 'key_words', 'img']:
                result = None
                exec('result = {}!={}'.format(ii, '\''+'NULL'+'\''))
                if result:
                    exec('{}=\'\\\'\' + {} + \'\\\'\''.format(ii, ii))
            sql = 'insert into news(title,date,newsabstract,refer_url,type,key_words,img) values('+title+','+date+',' + Abstract+','+refer_url+','+NewsType+','+key_words+','+img+');'
            res = cursor.execute(sql)
            insert_id = 'SELECT LAST_INSERT_ID();'
            cursor.execute(insert_id)
            db.commit()
            NewsId = str(cursor.fetchone()[0])
            html, md = WEB.GetNewsArticle(tag)
            with open(fbh+NewsId+'.html', 'w', encoding='utf-8') as f:
                f.write(html)
            with open(fbm+NewsId+'.md', 'w', encoding='utf-8') as f:
                f.write(md)
    db.close()
