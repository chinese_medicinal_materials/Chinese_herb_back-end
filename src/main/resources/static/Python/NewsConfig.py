from RootNewsConfig import RootNewsConfig


class yaocaizhongziYSDT(RootNewsConfig):
    '''中药材种植指南网_药市动态'''
    url = 'http://www.yaocaizhongzi.com/ysdt/ysdt.htm'
    root_url = 'http://www.yaocaizhongzi.com/ysdt/'  # 新闻详情页面的root_url
    NewsType = '药市动态'
    NewsListConfig = {
        'listloc': {
            'name': 'table',
            'attrs': {'border': '0', 'width': '92%', 'align': 'center', 'cellspacing': '5'}},
        'Newsloc': {
            'name': 'a',
            'attrs': {'target': '_blank', 'class': 'STYLE14'}
        }
    }
    ArticleHTMLTag = {
        'name': 'table',
        'attrs': {'align': 'center', 'width': '96%', 'border': '0'}
    }

# class yaocaizhongziFZPZ(RootNewsConfig):
#     '''中药材种植指南网_发展品种'''
#     url = 'http://www.yaocaizhongzi.com/fzpz/fzpz.htm'
#     root_url = 'http://www.yaocaizhongzi.com/fzpz/'
#     NewsType = '发展品种'
#     NewsListConfig = {
#         <td valign="top" bgcolor="#FFFFFF">
#     }
