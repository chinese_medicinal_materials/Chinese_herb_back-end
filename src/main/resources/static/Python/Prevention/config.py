# -*- coding: utf-8 -*-
'''
2020.5.17 author :Huangyi

'''

Disease = ["AppleBlackStar1", "AppleBlackStar2", "AppleGraySpot"]

Area = ["Beijing", "Tianjin", "Hebei", "Liaoning", "Jilin", "Heilongjiang", "Shandong", "Jiangsu", "Shanghai",
        "Zhejiang", "Anhui", "Fujian", "ShanXi", "Guangdong", "Guangxi", "Hainan", "Henan", "Hunan", "Hubei",
        "Shanxi", "Neimenggu", "Ningxia", "Qinghai", "Shanxi", "Gansu", "Xinjiang", "Sichuan", "Guizhou", "Ynnan",
        "Chongqing", "Xizang", "Xianggang", "Aomen", "Taiwan", ]

Suggest = ["常规生产规程中所采用的化学药，大多具有抑制生长甚至伤害作物的特征，其作用机理只能通过杀菌来达到防病治病的效果，不具有营养复壮的作用。"
           "而靓果安是药食同源、药肥双效，营养复壮，有利于展叶，叶绿体创造的叶绿素多，创造的有机质多，树（株）体必然健壮。果实后，要及时进行喷药防治。"
           "在6月中旬至8月期间，要视降雨和田间发生情况，每间隔15天左右喷药防治一次。果园病害尚未发生前，要喷洒好保护性杀菌剂，" ,
           "加强栽培管理。增施有机肥，低洼积水地注意及时排水，改良土壤，以增强树势",
           "清除初侵染源。挖除果园内重病树、病死树、根蘖苗，清除病根，锯除发病枝干，及时刮除病苗子实体集中烧毁或深埋。喷干枝清园，全树喷洒300倍液+有机硅。",
           "病害发生后应配合使用内吸性杀菌剂。保护性杀菌剂可选用1：2.5～3：200倍波尔多液，或50%代森锰锌可湿性粉剂500～600倍液、或80%大生M－45可湿性粉剂600－800倍液等；"
           "内吸性杀菌剂可选用25%戊唑醇可湿性粉剂2000～2500倍液、或10%苯醚甲环唑水分散颗粒剂1500～2500倍液、或70%甲基硫菌灵可湿性粉剂800～1000倍液、"
           "或50%多菌灵可湿性粉剂800～1000倍液。喷药要周到细致，树冠内外、上下和叶片的正反两面都要均匀着药"]