package com.herb.utils;

public class result {
    private int code;

    public result(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    //<QiuJintao>
    private String Message = null;
    private Object data = null;
    public result(int code,String message){
        this.code = code;
        this.Message = message;
    }
    public result(int code,String message,Object data){
        this.code = code;
        this.Message = message;
        this.data = data;
    }
    public String getMessage(){
        return Message;
    }

    public Object getData() {
        return data;
    }

    //</QiuJintao>
}
