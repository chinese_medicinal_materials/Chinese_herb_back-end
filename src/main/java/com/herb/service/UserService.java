package com.herb.service;

import com.alibaba.fastjson.JSON;
import com.herb.dao.RoleMapper;
import com.herb.dao.UserMapper;
import com.herb.dao.UserRoleMapper;
import com.herb.domain.*;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private RoleMapper roleMapper;

    //得到所有用户
    public List<User> getUserList() {
        return userMapper.selectByExample(new UserExample());
    }



    //新用户注册
    public Integer saveUser(User user) {
        return userMapper.insert(user);
    }


    //根据
    public List<Role> findRoles(Integer id) {
        UserRoleExample example = new UserRoleExample();
        example.createCriteria().andUseridEqualTo(id);
        List<UserRole> keyList = userRoleMapper.selectByExample(example);

        System.out.println(JSON.toJSONString(keyList));

        List<Integer> roleIdList = new ArrayList<>(keyList.size());

        for (UserRole userRole : keyList) {
            roleIdList.add(userRole.getRoleid());
        }

        RoleExample roleExample = new RoleExample();
        roleExample.createCriteria().andRoleidIn(roleIdList);

        System.out.println(JSON.toJSONString(roleMapper.selectByExample(roleExample)));
        return roleMapper.selectByExample(roleExample);

    }


    public User findByName(String username) {
        return userMapper.findByName(username);
    }

    /**
     * @author QiuJIntao
     * @date 2020/4/21
     * @return
     */
    public User FindByPhoneOrEmail(String userinfo) {
        userinfo = HtmlUtils.htmlEscape(userinfo);
        String Email_Pattern = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        String Phone_Pattern = "1\\d{10}";
        boolean Email_Match = Pattern.matches(Email_Pattern, userinfo);
        boolean Phone_Match = Pattern.matches(Phone_Pattern, userinfo);
        if(Email_Match){
            return userMapper.findByEmail(userinfo);
        }else if(Phone_Match){
            return userMapper.findByPhone(userinfo);
        }else {
            return null;
        }
    }
    public boolean isExist(String userinfo) {
        User user = FindByPhoneOrEmail(userinfo);
        return null != user;
    }
    public User HtmlEscapeInfo(User user){
        String username = user.getUsername();
        String phone = user.getPhone();
        String email = user.getEmail();

        username = HtmlUtils.htmlEscape(username);
        user.setUsername(username);
        phone = HtmlUtils.htmlEscape(phone);
        user.setPhone(phone);
        email = HtmlUtils.htmlEscape(email);
        user.setEmail(email);
        return user;
    }
    public int register(User user) {
        user = HtmlEscapeInfo(user);
        //检查填写是否正确
        if (user.getUsername().length()==0 || user.getPassword().length()==0) {
            return 1;
        }
        if (user.getPhone().length()==0 && user.getEmail().length()==0) {
            return 2;
        }
        //格式检查
        String Email_Pattern = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        String Phone_Pattern = "1\\d{10}";
        boolean Email_Match = Pattern.matches(Email_Pattern, user.getEmail());
        boolean Phone_Match = Pattern.matches(Phone_Pattern, user.getPhone());
        if(user.getEmail().length()!=0 && !Email_Match){
            return 3;
        }
        if(user.getPhone().length()!=0 && !Phone_Match){
            return 4;
        }
        //是否已被注册
        if(isExist(user.getPhone())){
            return 5;
        }else if(isExist(user.getEmail())){
            return 6;
        }
        // 默认生成 24 位盐
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        ByteSource newsalt =ByteSource.Util.bytes(salt);
        int times = 2;
        String encodedPassword = new SimpleHash("md5", user.getPassword(), newsalt, times).toString();

        user.setSalt(salt);
        user.setPassword(encodedPassword);

        userMapper.insertSelective(user);
        //创建用户角色
        UserRole userrole = new UserRole();
        userrole.setRoleid(2);
        User usr = null;
        if(user.getEmail().length()!=0){
            usr = FindByPhoneOrEmail(user.getEmail());
        }else if(user.getPhone().length()!=0){
            usr = FindByPhoneOrEmail(user.getPhone());
        }
        userrole.setUserid(usr.getUserid());
        userRoleMapper.insert(userrole);
        return 0;
    }

    public User updateUserInfo(User user) {
        int userid = user.getUserid();
        String Email = user.getEmail();
        String Phone = user.getPhone();
        String Username = user.getUsername();
        userMapper.updateUserInfo(userid,Email,Phone,Username);
        User updateduser = userMapper.selectByPrimaryKey(userid);
        return updateduser;
    }


    public int changePassword(int userid, String oldPwd, String newPwd) {
       User user = userMapper.selectByPrimaryKey(userid);
       String salt = user.getSalt();
       ByteSource newsalt =ByteSource.Util.bytes(salt);
       String pwd = user.getPassword();
       System.out.println(oldPwd);
        System.out.println(newPwd);
       System.out.println(JSON.toJSONString(user));
       int times = 2;
       String encodedOldPwd = new SimpleHash("md5", oldPwd,newsalt, times).toString();
       System.out.println(encodedOldPwd);
       if (pwd.equals(encodedOldPwd)){
           String encodedNewPwd = new SimpleHash("md5", newPwd,newsalt, times).toString();
           System.out.println(encodedNewPwd);
           userMapper.updatePasswordByUserID(userid,encodedNewPwd);
           return 1;
       }
       else{
           return 0;
       }
    }
}
