package com.herb.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.herb.dao.NewsMapper;
import com.herb.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

/**
 * @author : QiuJintao
 * @description : TODO
 * @date : 2020/5/14 16:39
 */
@Service
public class NewsService {
    @Autowired
    private NewsMapper newsMapper;

    //当前文章是否存在
    public Boolean isExist(News newsarticle){
        if(newsarticle.getId()!=null){
            System.out.println("Id已存在");
            return newsMapper.selectByPrimaryKey(newsarticle.getId())!=null;
        }else {
            System.out.println("Id不存在");
            return false;
        }
    }

    public void addOrUpdate(News newsarticle){
        int newsId;
        String Html = newsarticle.getArticlecontenthtml();
        String Md = newsarticle.getArticlecontentmd();
        newsarticle.setArticlecontenthtml("");
        newsarticle.setArticlecontentmd("");
        if(!isExist(newsarticle)){
            System.out.println("新文件！！");
            newsMapper.insertSelective(newsarticle);
            newsId = newsMapper.selectLastInsertId();
        }else {
            newsMapper.updateByPrimaryKey(newsarticle);
            newsId = newsarticle.getId();
        }
        System.out.println(newsId);
        newsarticle.setArticlecontentmd(Md);
        newsarticle.setArticlecontenthtml(Html);
        newsarticle.setId(newsId);
        setNewsHtmlMd(newsarticle);
    }

    public News FindById(int id){
        return newsMapper.selectByPrimaryKey(id);
    }

    public List<News> findAll(){
        return newsMapper.selectAll();
    };

    public PageResult findPage(PageRequest pageRequest){
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    };

    private PageInfo<News> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<News> sysMenus = newsMapper.selectPage();
        return new PageInfo<News>(sysMenus);
    }

    public String getNewsHtml(int id){
        String NewsLoc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/News/");
        NewsLoc = NewsLoc.substring(1);

        String NewsHTML = "";
        String line;
        try{
            String HTMLLoc = NewsLoc+"HTML/"+id+".html";
            FileReader HTMLReader = new FileReader(HTMLLoc);
            BufferedReader brHTML = new BufferedReader(HTMLReader);
            while ((line = brHTML.readLine()) != null) {
                NewsHTML = NewsHTML + line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return NewsHTML;
    }

    public String getNewsMd(int id){
        String NewsLoc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/News/");
        NewsLoc = NewsLoc.substring(1);
        String NewsMd = "";
        String line;
        try{
            String MdLoc = NewsLoc+"MD/"+id+".md";
            FileReader MDReader = new FileReader(MdLoc);
            BufferedReader brMd = new BufferedReader(MDReader);
            while ((line = brMd.readLine()) != null) {
                NewsMd = NewsMd + line;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return NewsMd;
    }
    public void setNewsHtmlMd(News article){
        String NewsLoc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/News/");
        NewsLoc = NewsLoc.substring(1);
        try{
            FileWriter HTMLWriter = new FileWriter(NewsLoc+"HTML/"+article.getId()+".html");
            HTMLWriter.write(article.getArticlecontenthtml());
            HTMLWriter.close();
            FileWriter MdWriter = new FileWriter(NewsLoc+"MD/"+article.getId()+".md");
            MdWriter.write(article.getArticlecontentmd());
            MdWriter.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void delete(int id){
        newsMapper.deleteByPrimaryKey(id);
        String NewsLoc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/News/");
        NewsLoc = NewsLoc.substring(1);
        String HtmlFile = NewsLoc+"HTML/"+id+".html";
        String mdFile = NewsLoc+"MD/"+id+".md";
        File file1 = new File(HtmlFile);// 读取
        System.out.println(HtmlFile);
        file1.delete();
        File file2 = new File(mdFile);
        file2.delete();
    }

}
