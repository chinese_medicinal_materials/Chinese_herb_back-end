package com.herb.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.herb.dao.EcosystemMapper;
import com.herb.dao.HerbDistributeMapper;
import com.herb.dao.HerbMapper;
import com.herb.dao.HerbYieldMapper;
import com.herb.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

@Service
public class HerbService {

    @Autowired
    private HerbMapper herbmapper;

    @Autowired
    private HerbYieldMapper herbYieldMapper;

    @Autowired
    private HerbDistributeMapper herbDistributeMapper;

    @Autowired
    private EcosystemMapper ecosystemMapper;

    public List<Herb> SearchHerb(String  keyword){

       return herbmapper.searchHerb(keyword);

    }

    public List<Herb> SearchIdbyHerbname(String  herbname){

        return herbmapper.searchIdByHerbName(herbname);

    }

    public int SaveHerb(Herb herb) {
        return herbmapper.insert(herb);
    }

    public int UpdateHerb(Herb herb) {
        return herbmapper.updateByPrimaryKey(herb);
    }

    public int DeleteHerb(Long herbid) {
        return herbmapper.deleteByPrimaryKey(herbid);
    }

    public int InsertHerbYield(HerbYield herbYield) {
        return herbYieldMapper.insert(herbYield);
    }

    public JSONArray getHerbNamelist() {


        JSONArray array = JSONArray.parseArray(JSON.toJSONString(herbmapper.getHerbNamelist()));

        List<Herb> herbnamelist = herbmapper.getHerbNamelist();


        System.out.println(array);
        return array ;
    }

    public int CountHerbNumberByCityName(String CityName) {
        System.out.println(CityName);
        return herbmapper.countHerbNumberByCityName(CityName);
    }

    public List<HerbYield> getLocationByHerbId(Long herbId) {
        return herbmapper.getLocationByHerbId(herbId);
    }

    public List<HerbYield> getHerbYieldByHerbNameAndLocation(Long herbId, String location) {
        return herbmapper.getHerbYieldByHerbNameAndLocation(herbId,location);
    }

    public Map<String,Object> SuitableAreaDiscovery(String herbname ) {
        System.out.println("------------------------------------------------");
        String Pyloc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
        Pyloc = Pyloc.substring(1) + "Python/SuitableAreaDiscovery/herb_distribution.py";
        System.out.println(Pyloc);
        String command = "python "+Pyloc+" "+herbname;
        String[] PyProcResult =new String[4];
        // TODO Auto-generated method stub
        Process proc;
        Process proc2;
        try {
            proc = Runtime.getRuntime().exec("python "+Pyloc+" "+herbname);// 执行py文件
            //用输入输出流来截取结果
            /*为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞*/
            Thread t=new Thread(new InputStreamRunnable(proc.getErrorStream(),"ErrorStream"));
            t.start();
            BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream(),"gb2312"));
            String line = null;
            int i = 0;
            while ((line = in.readLine()) != null) {
                PyProcResult[i] = line;
                i++;
            }
            System.out.println(PyProcResult[0]);
            System.out.println(PyProcResult[1]);
            System.out.println(PyProcResult[2]);
            System.out.println(PyProcResult[3]);
            in.close();
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        File file = new File(PyProcResult[2]);
        BufferedReader reader = null;
        List<String> cityList = new LinkedList<String >();
        List<Map<String,Object>> reportInfomation = new LinkedList<>();
        Map<String,Object> locationMap = new LinkedHashMap<>();
        Map<String,Object> map =new HashMap<String,Object>();
        try {
            InputStreamReader read = new InputStreamReader(new FileInputStream(file),
                    "GBK");
            reader = new BufferedReader(read);
            String tempString = null;
            int line = 1;
            // 一次读一行，读入null时文件结束
            while ((tempString = reader.readLine()) != null) {
                String[] split = tempString.split(",");
                Long ecosystem_id =Long.parseLong(split[0]);
                Double longitude =Double.parseDouble(split[1]);
                Double latitude =Double.parseDouble(split[2]);
                String province =split[3];
                String  city =split[4];
                String  district =split[5];
                List<Double> long_lat = new LinkedList<>();
                long_lat.add(longitude);
                long_lat.add(latitude);
                locationMap.put(province+ecosystem_id,long_lat);
                cityList.add(province+ecosystem_id);
                Map<String,Object> pointmap =new HashMap<String,Object>();
                pointmap.put("jingwei","["+longitude+","+latitude+"]");
                pointmap.put("area",province+city+district);
                reportInfomation.add(pointmap);
                //Ecosystem ecosystem = ecosystemMapper.selectBLonAndLat(longitude,latitude);
                line++;
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        map.put("LocationMap",locationMap);
        map.put("cityList",cityList);
        map.put("reportInfomation",reportInfomation);
        return map;
        //System.out.println(JSON.toJSONString(ecosystem));
    }

    public Ecosystem findEcosystemByLonAndLat(Double longitude, Double latitude) {
        List<Ecosystem> list = herbmapper.findEcosystemByLonAndLat(longitude,latitude);
        if (list.size() == 1){
            return list.get(0);
        }  else{
            return new Ecosystem();
        }
  /*      for (Ecosystem ecosystem :list){
            return ecosystem;
        }*/
    }

    public void InsertHerbDistribution(HerbDistribute herbDistribute) {
        herbDistributeMapper.insert(herbDistribute);
    }

    public List<HerbDistribute> findDistribution(String keyword,Integer userId) {
        return herbDistributeMapper.findDistributionById(keyword,userId);
    }

    public List<HerbDistribute> findDistributionByHerbName(String herbName) {
        return herbDistributeMapper.findDistributionByHerbName(herbName);
    }

    public void UpdateDistribution(HerbDistribute params) {
        Double longitude = params.getLongitude();
        Double latitude = params.getLatitude();
        List<Ecosystem> list = herbmapper.findEcosystemByLonAndLat(longitude,latitude);
        Ecosystem ecosystem = new Ecosystem();
        if (list.size() == 1){
             ecosystem = list.get(0);
        }
        System.out.println(JSON.toJSONString(list));
        params.setEcosystemId(ecosystem.getEcosystemId());
        params.setAvgTempAnn(ecosystem.getAvgTempAnn());
        params.setAvgTempDayRange(ecosystem.getAvgTempDayRange());
        params.setIsothermality(ecosystem.getIsothermality());
        params.setTempSeasonality(ecosystem.getTempSeasonality());
        params.setMaxTempWarmestMonth(ecosystem.getMaxTempWarmestMonth());
        params.setMinTempColdestMonth(ecosystem.getMinTempColdestMonth());
        params.setTempAnnRange(ecosystem.getTempAnnRange());
        params.setAvgTempWettestQuarter(ecosystem.getAvgTempWettestQuarter());
        params.setAvgTempDriestQuarter(ecosystem.getAvgTempDriestQuarter());
        params.setAvgTempWarmestQuarter(ecosystem.getAvgTempWarmestQuarter());
        params.setAvgTempColdestQuarter(ecosystem.getAvgTempColdestQuarter());
        params.setPrecAnn(ecosystem.getPrecAnn());
        params.setPrecWettestMonth(ecosystem.getPrecWettestMonth());
        params.setPrecDriestMonth(ecosystem.getPrecDriestMonth());
        params.setPrecSeasonality(ecosystem.getPrecSeasonality());
        params.setPrecWarmestQuarter(ecosystem.getPrecWarmestQuarter());
        params.setPrecDriestQuarter(ecosystem.getPrecDriestQuarter());
        params.setPrecWarmestQuarter(ecosystem.getPrecWarmestQuarter());
        params.setPrecColdestQuarter(ecosystem.getPrecColdestQuarter());
        params.setPrecWettestMonth(ecosystem.getPrecWettestMonth());
        params.setPrecWettestQuarter(ecosystem.getPrecWettestQuarter());
        params.setProvince(ecosystem.getProvince());
        params.setCity(ecosystem.getCity());
        params.setDistrict(ecosystem.getDistrict());
        System.out.println(JSON.toJSONString(params));
        herbDistributeMapper.updateByPrimaryKey(params);
    }

    public HerbDistribute getUpdateDistribution(Long distributionId) {
       return herbDistributeMapper.selectByPrimaryKey(distributionId);
    }

    public void deleteDistribution(Long distributionId) {
        herbDistributeMapper.deleteByPrimaryKey(distributionId);
    }

    public List<Ecosystem> ReverseGeocoding() {
       return ecosystemMapper.ReverseGeocoding();
    }

    public void UpdateEcosystem(Ecosystem data) {
        ecosystemMapper.updateByPrimaryKey(data);
    }

    public List<HerbDistribute> findDistributionPage(String query, int userId, int pagenum, int pagesize) {
        System.out.println(query);
        return herbDistributeMapper.findDistributionPage(query,userId,(pagenum-1)*pagesize,pagesize);
    }

    public HerbDistribute findDistributionByPK(Long distributionId) {
        return herbDistributeMapper.selectByPrimaryKey(distributionId);
    }

    public List<HerbDistribute> searchDistribution(String keyword) {
        return herbDistributeMapper.searchDistributionByKey(keyword);
    }

    public List<HerbDistribute> getAllDistribution(){
        return herbDistributeMapper.selectByExample(new HerbDistributeExample());
    }


    class InputStreamRunnable implements Runnable {
        BufferedReader bReader=null;
        String type=null;
        public InputStreamRunnable(InputStream is, String _type) {
            try {
                bReader=new BufferedReader(new InputStreamReader(new BufferedInputStream(is),"UTF-8"));
                type=_type;
            } catch(Exception ex) {}
        }
        public void run() {
            String line;
            try {
                while((line=bReader.readLine())!=null) {
                    System.out.println("err>>"+line);
                    //Thread.sleep(200);
                }
                bReader.close();
            } catch(Exception ex) {}
        }
    }
}
