package com.herb.dao;

import com.herb.domain.HerbDistribute;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HerbDistributeMapperExt {
    List<HerbDistribute> findDistributionById(@Param("keyword") String keyword,@Param("userId") Integer userId);

    List<HerbDistribute> findDistributionByHerbName(@Param("herbName")String herbName);

    List<HerbDistribute> findDistributionPage(@Param("keyword")String query,@Param("userId")int userId, @Param("currIndex")int currIndex,@Param("pagesize")int pagesize);

    List<HerbDistribute> searchDistributionByKey(@Param("keyword")String keyword);
}
