package com.herb.dao;

import com.herb.domain.Ecosystem;
import com.herb.domain.Herb;
import com.herb.domain.HerbDistribute;
import com.herb.domain.HerbYield;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HerbMapperExt {

    List<Herb> searchHerb (@Param("keyword") String  keyword);

    List<Herb> searchIdByHerbName (@Param("herbname") String  herbname);

    List<Herb> getHerbNamelist();

    int countHerbNumberByCityName(@Param("provinceName") String  provinceName);

    List<HerbYield> getLocationByHerbId(@Param("herbId")Long herbId);

    List<HerbYield> getHerbYieldByHerbNameAndLocation(@Param("herbId")Long herbId,@Param("location")String location);

    List<HerbDistribute> getTrainData();

    List<Ecosystem> getTestData();

    List<Ecosystem> findEcosystemByLonAndLat(@Param("longitude")Double longitude,@Param("latitude") Double latitude);
}
