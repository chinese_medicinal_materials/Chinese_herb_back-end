package com.herb.dao;
import com.herb.domain.News;

import java.util.List;

/**
 * @author : QiuJintao
 * @description : TODO
 * @date : 2020/5/15 12:34
 */
public interface NewsMapperExt {
    List<News> selectAll();
    List<News> selectPage();
    int selectLastInsertId();
}
