package com.herb.dao;

import com.herb.domain.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapperExt {
    User findByName(@Param("username")String username);
    User findByEmail(String email);
    User findByPhone(String phone);

    void updateUserInfo(@Param("userid")int userid,
                        @Param("email")String Email,
                        @Param("phone")String Phone,
                        @Param("username")String Username);

    void updatePasswordByUserID(@Param("userid")int userid, @Param("newPwd")String encodedNewPwd);
}
