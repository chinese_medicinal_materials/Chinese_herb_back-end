package com.herb.dao;

import com.herb.domain.Ecosystem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EcosystemMapperExt {
    List<Ecosystem> ReverseGeocoding();

    Ecosystem selectBLonAndLat(@Param("longitude")Double longitude,@Param("latitude")Double latitude);
}
