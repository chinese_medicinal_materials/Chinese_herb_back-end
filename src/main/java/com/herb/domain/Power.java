package com.herb.domain;

public class Power {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column power.ID
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column power.PowerType
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    private String powertype;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column power.ID
     *
     * @return the value of power.ID
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column power.ID
     *
     * @param id the value for power.ID
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column power.PowerType
     *
     * @return the value of power.PowerType
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    public String getPowertype() {
        return powertype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column power.PowerType
     *
     * @param powertype the value for power.PowerType
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    public void setPowertype(String powertype) {
        this.powertype = powertype == null ? null : powertype.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table power
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Power other = (Power) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getPowertype() == null ? other.getPowertype() == null : this.getPowertype().equals(other.getPowertype()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table power
     *
     * @mbg.generated Mon Jun 08 22:16:51 CST 2020
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPowertype() == null) ? 0 : getPowertype().hashCode());
        return result;
    }
}