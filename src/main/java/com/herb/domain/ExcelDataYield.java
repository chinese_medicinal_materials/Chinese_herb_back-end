package com.herb.domain;

public class ExcelDataYield {

    private String column1;

    private Integer column2;

    private String column3;

    private Double column4;

    @Override
    public String toString() {
        return "ExcelDataYield [column1=" + column1 + ", column2=" + column2 + ", column3=" + column3 + ", column4=" + column4 + "]";
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public Integer getColumn2() {
        return column2;
    }

    public void setColumn2(Integer column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public Double getColumn4() {
        return column4;
    }

    public void setColumn4(Double column4) {
        this.column4 = column4;
    }

}
