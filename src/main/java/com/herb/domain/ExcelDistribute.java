package com.herb.domain;

public class ExcelDistribute {
    private String herbName;//药材名

    private Double longitude;//经度

    private Double latitude;//纬度

    @Override
    public String toString() {
        return "ExcelDistribute [药材名=" + herbName + ", 经度=" + longitude + ", 纬度=" + latitude+ "]";
    }

    public void setHerbName(String herbName) {
        this.herbName = herbName;
    }

    public String getHerbName() {
        return herbName;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


}
