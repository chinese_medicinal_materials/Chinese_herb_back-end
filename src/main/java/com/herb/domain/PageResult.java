package com.herb.domain;

import java.util.List;

/**
 * @author : QiuJintao
 * @description : TODO
 * @date : 2020/5/17 20:10
 */
public class PageResult {
    /**
     * 当前页码
     */
    private int pageNum;
    /**
     * 每页数量
     */
    private int pageSize;
    /**
     * 记录总数
     */
    private long totalSize;
    /**
     * 页码总数
     */
    private int totalPages;
    /**
     * 当前页面记录总数
     */
    private int totalElements;
    /**
     * 数据模型
     */
    private List<News> content;
    public int getPageNum() {
        return pageNum;
    }
    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    public long getTotalSize() {
        return totalSize;
    }
    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }
    public int getTotalPages() {
        return totalPages;
    }
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
    public List<News> getContent() {
        return content;
    }
    public void setContent(List<News> content) {
        this.content = content;
    }
    public void setTotalElements(int totalElements){this.totalElements = totalElements;}
    public int getTotalElements(){return totalElements;}
}
