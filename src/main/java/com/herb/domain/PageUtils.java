package com.herb.domain;
import com.github.pagehelper.PageInfo;
/**
 * @author : QiuJintao
 * @description : TODO
 * @date : 2020/5/17 20:11
 */
public class PageUtils {
    public static PageResult getPageResult(PageRequest pageRequest, PageInfo<News> pageInfo) {
        PageResult pageResult = new PageResult();
        pageResult.setPageNum(pageInfo.getPageNum());
        pageResult.setPageSize(pageInfo.getPageSize());
        pageResult.setTotalSize(pageInfo.getTotal());
        pageResult.setTotalPages(pageInfo.getPages());
        pageResult.setContent(pageInfo.getList());
        if(pageRequest.getPageNum()<pageResult.getTotalPages()){
            pageResult.setTotalElements(pageRequest.getPageSize());
        }else {
            pageResult.setTotalElements((int) (pageInfo.getTotal() % pageResult.getPageSize()));
        }
        return pageResult;
    }
}
