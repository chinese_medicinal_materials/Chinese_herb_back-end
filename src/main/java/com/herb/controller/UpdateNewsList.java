package com.herb.controller;
/**
 * @author QiuJIntao
 * @date 2020/4/26
 * @return
 * 此程序用于定时调用python模块以更新新闻列表
 * 状态：未完成
 */
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class UpdateNewsList implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.addTriggerTask(
                ()->{//调用Python更新新闻
                    Process proc;
                    try{
                        String Pyloc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
                        Pyloc = Pyloc.substring(1) + "Python/WebCrawler.py";
                        proc = Runtime.getRuntime().exec("python "+Pyloc);
                        /*为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞*/
                        Thread t=new Thread(new InputStreamRunnable(proc.getErrorStream(),"ErrorStream"));
                        t.start();
                        BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                        String line = null;
                        while ((line = in.readLine()) != null) {
                            System.out.println(line);
                        }
                        in.close();
                        proc.waitFor();
                    }catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } },
                triggerContext -> {
                    String cron = "0 0 1 * * ?";
                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
                }
        );
    }
}

class InputStreamRunnable implements Runnable {
    BufferedReader bReader=null;
    String type=null;
    public InputStreamRunnable(InputStream is, String _type) {
        try {
            bReader=new BufferedReader(new InputStreamReader(new BufferedInputStream(is),"UTF-8"));
            type=_type;
        } catch(Exception ex) {}
    }
    public void run() {
        String line;
        try {
            while((line=bReader.readLine())!=null) {
                System.out.println("err>>"+line);
                //Thread.sleep(200);
            }
            bReader.close();
        } catch(Exception ex) {}
    }
}
//test

