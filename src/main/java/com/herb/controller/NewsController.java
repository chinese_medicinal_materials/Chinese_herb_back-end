package com.herb.controller;

import com.alibaba.fastjson.JSON;
import com.herb.domain.PageRequest;
import com.herb.domain.PageResult;
import com.herb.service.NewsService;
import com.herb.utils.result;
import com.herb.domain.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author : QiuJintao
 * @description : TODO
 * @date : 2020/5/12 13:32
 */


@Controller
public class NewsController {
    @Autowired
    private NewsService newsservice;

    @CrossOrigin
    @PostMapping("api/admin/content/article")
    @ResponseBody
    public result saveArticle(@RequestBody News article) {
        newsservice.addOrUpdate(article);
        return new result(200, "保存成功");
    }

    @CrossOrigin
    @GetMapping("/api/article/{size}/{page}")
    @ResponseBody
    public result listArticles(@PathVariable("size") int size, @PathVariable("page") int page,@RequestParam(value="Newstype",required=false) String Newstype) {
        System.out.println(Newstype);
        PageRequest pageQuery = new PageRequest();
        pageQuery.setPageNum(page);
        pageQuery.setPageSize(size);
        PageResult pageResult = newsservice.findPage(pageQuery);
        List<News> newsResults = pageResult.getContent();
        for(News ii:newsResults){
            ii.setArticlecontentmd(newsservice.getNewsMd(ii.getId()));
            ii.setArticlecontenthtml(newsservice.getNewsHtml(ii.getId()));
        }
        pageResult.setContent(newsResults);
        System.out.println(JSON.toJSONString(pageResult));
        return new result(200,"成功",pageResult);
    }
//
    @CrossOrigin
    @GetMapping("/api/article/{id}")
    @ResponseBody
    public result getOneArticle(@PathVariable(name="id") int id) {
        System.out.println(id);
        News NewsArticle = newsservice.FindById(id);
        NewsArticle.setArticlecontentmd(newsservice.getNewsMd(NewsArticle.getId()));
        NewsArticle.setArticlecontenthtml(newsservice.getNewsHtml(NewsArticle.getId()));
        System.out.println(JSON.toJSONString(NewsArticle));
        return new result(200,"成功",NewsArticle);
    }
//
    @CrossOrigin
    @DeleteMapping("/api/admin/content/article/{id}")
    @ResponseBody
    public result deleteArticle(@PathVariable("id") int id) {
        newsservice.delete(id);
        return new result(200,"删除成功");
    }

    @CrossOrigin
    @PostMapping("/api/admin/content/article/coverimg")
    @ResponseBody
    public String coversUpload(MultipartFile file)throws IOException {
        System.out.println("getimg!");
        String NewsLoc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/News/");
        NewsLoc = NewsLoc.substring(1);
        String folder = NewsLoc+ "img";
        File imageFolder = new File(folder);
        //getRandomString
        int length = 6;
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        //
        File f = new File(imageFolder, sb.toString() + file.getOriginalFilename()
                .substring(file.getOriginalFilename().length() - 4));
        if (!f.getParentFile().exists())
            f.getParentFile().mkdirs();
        try {
            file.transferTo(f);
            String imgURL = "http://localhost:8443/News/img/" + f.getName();
            return imgURL;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * @author : Zhangchi
     * @description : TODO
     * @date : 2020/7/1 10:28
     */
    @CrossOrigin
    @PostMapping("/api/getnews")
    @ResponseBody
    public List<Map<String,Object>> getNews() {
        List<News> allList= newsservice.findAll();
        List<News> newsList;
        List<Map<String,Object>> List = new LinkedList<>();
        if (allList.size() > 5){
            newsList = allList.subList(0, 4);
        }else{
            newsList = allList;
       }
        for (News New:newsList){
            Map<String,Object> map= new LinkedHashMap<String,Object>();
            String[] titleArray = New.getTitle().split("\\r");
            map.put("title",titleArray[0]);
            map.put("id",New.getId());
            List.add(map);
        }
        return List;
    }


}
