package com.herb.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.herb.domain.ExcelDataYield;
import com.herb.domain.Herb;
import com.herb.domain.HerbYield;
import com.herb.service.HerbService;
import com.herb.utils.ExcelUtil;
import com.herb.utils.LocalFileUtil;
import com.herb.utils.result;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@CrossOrigin
@RestController
public class FileController {

    @Autowired
    private HerbService herbservice;

    @PostMapping("/api/uploadwork")
    public String uploadWork(HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) throws IOException {

        request.setCharacterEncoding("UTF-8");
        String user = request.getParameter("user");
        System.out.println(user);


        if(!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = null;
            String type = fileName.indexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : null;


            if (type != null) {

                if ("DOCX".equals(type.toUpperCase())||"DOC".equals(type.toUpperCase())) {
                    // 项目在容器中实际发布运行的根路径

                    String realPath = request.getSession().getServletContext().getRealPath("/");
                    // 自定义的文件名称
                    String trueFileName = user + "_" + fileName;

                    // 设置存放文件的路径
                    path = "E:/workplace/classwork/" + trueFileName;
                    File dest = new File(path);
                    //判断文件父目录是否存在
                    if (!dest.getParentFile().exists()) {
                        dest.getParentFile().mkdir();
                    }

                    file.transferTo(dest);

                    return trueFileName;
                }else {
                    return "error";
                }
            }else {
                return "error";
            }
        }else {
            return "error";
        }
    }

    @PostMapping("/api/getherbnamelist")
    public Map<String,Object> Getherbnamelist(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        //JSONObject jsonObject = new JSONObject();
        //herbservice.getHerbNamelist();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("herbnamelist",  herbservice.getHerbNamelist());
        return map;

    }

    @PostMapping("/api/uploadexcel")
    public String uploadExcel(HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) throws IOException{
        File f = null;
        try {
            // MultipartFile 转  file
            f = LocalFileUtil.multipartFileToFile(file);
            List<ExcelDataYield> list = ExcelUtil.importExcel(f, ExcelDataYield.class, 1);
            for (ExcelDataYield data : list) {
                // 这里是保存数据库操作

                //首先根据药材名查询对应的药材ID
                List<Herb> herblist= herbservice.SearchIdbyHerbname(data.getColumn1());
                for (Herb herb :herblist){

                    HerbYield herbYield = new HerbYield();
                    herbYield.setHerbId(herb.getHerbId());
                    herbYield.setHarvestYear(data.getColumn2());
                    herbYield.setLocation(data.getColumn3());
                    herbYield.setYield(data.getColumn4());
                    System.out.println(JSON.toJSONString(herbYield));
                    herbservice.InsertHerbYield(herbYield);
                }

                System.out.println(data.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (f != null) {
                f.delete();
            }
        }

        return "success";
    }

    @GetMapping("/api/downloadTemplateExcel")
    public String download(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        String fileName = "distributionTemplateExcel.xlsx";
        String filePath = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/Template/");
        System.out.println(1234567);

        File file = new File(filePath + "/" + fileName);
        if(file.exists()){ //判断文件父目录是否存在
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            // response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment;fileName=" +   java.net.URLEncoder.encode(fileName,"UTF-8"));
            byte[] buffer = new byte[1024];
            FileInputStream fis = null; //文件输入流
            BufferedInputStream bis = null;

            OutputStream os = null; //输出流
            try {
                os = response.getOutputStream();
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                int i = bis.read(buffer);
                while(i != -1){
                    os.write(buffer);
                    i = bis.read(buffer);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("----------file download---" + fileName);
            try {
                bis.close();
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    @RequestMapping(value = "/api/uploadbase64", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> saveBase64(@RequestBody String diaLogForm/*,@RequestParam(value = "img") String base64Str*/, HttpServletRequest request, HttpServletResponse response) {
        System.out.println(diaLogForm);
        System.out.println(JSON.parse(diaLogForm));
        JSONObject jsonObject = (JSONObject) JSON.parse(diaLogForm);
        JSONObject diaLogform = (JSONObject) jsonObject.get("diaLogForm");
        JSONArray imgBroadcastList = (JSONArray) diaLogform.get("imgBroadcastList");
        String base64Str = (String) diaLogform.get("imgsStr");
        System.out.println(imgBroadcastList);
        System.out.println(base64Str);

        Map<String,Object> map1 = new HashMap<String,Object>();

        for (int i = 0; i < imgBroadcastList.size(); i++) {
            System.out.println(imgBroadcastList.getJSONObject(i));
        }

        String dataPrix = ""; //base64格式前头
        String data = "";//实体部分数据
        if (base64Str == null || "".equals(base64Str)) {
            // return new result(200, "传送的图片为空");
        } else {
            String[] d = base64Str.split("base64,");//将字符串分成数组
            System.out.println(d[0]);
            if (d != null && d.length == 2) {
                dataPrix = d[0];
                data = d[1];
            } else {
                //return new result(401, "传送数据不合法");
            }
        }
        String suffix = "";//图片后缀，用以识别哪种格式数据
        //data:image/jpeg;base64,base64编码的jpeg图片数据
        if ("data:image/jpeg;".equalsIgnoreCase(dataPrix)) {
            suffix = ".jpg";
        } else if ("data:image/x-icon;".equalsIgnoreCase(dataPrix)) {
            //data:image/x-icon;base64,base64编码的icon图片数据
            suffix = ".ico";
        } else if ("data:image/gif;".equalsIgnoreCase(dataPrix)) {
            //data:image/gif;base64,base64编码的gif图片数据
            suffix = ".gif";
        } else if ("data:image/png;".equalsIgnoreCase(dataPrix)) {
            //data:image/png;base64,base64编码的png图片数据
            suffix = ".png";
        } else {
            //return new result(401, "图片格式不合法");
        }
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String tempFileName = uuid + suffix;

        String imgFilePath = "D:/HerbTrain/AgriculturalDisease_trainA/images/" + tempFileName;//新生成的图片

        String imgFilePath1 = "D:/HerbTrain/AgriculturalDisease_Save/images/" + tempFileName;//保存的图片

        BASE64Decoder decoder = new BASE64Decoder();
        try {
            //Base64解码
            byte[] b = decoder.decodeBuffer(data);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    //调整异常数据
                    b[i] += 256;
                }
            }
            OutputStream out = new FileOutputStream(imgFilePath);

            out.write(b);
            out.flush();
            out.close();

            //System.out.println(123);
            //执行判断是否图片库有相同图片
            Process proc;
            try {
                //proc = Runtime.getRuntime().exec("D:\\CodeSoft\\Anaconda\\envs\\tf-gpu\\python C:\\Users\\Administrator\\PycharmProjects\\Agricultural-Disease-Classification-master\\resnet50\\Compare.py");// 执行py文件
                //用输入输出流来截取结果
                String Pyloc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
                Pyloc = Pyloc.substring(1) + "Python/Disease/resnettest.py";
                proc = Runtime.getRuntime().exec("E:\\Anaconda\\envs\\tf-gpu\\python "+Pyloc);
                /*为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞*/
                Thread t=new Thread(new InputStreamRunnable(proc.getErrorStream(),"ErrorStream"));
                t.start();
                BufferedReader in1 = new BufferedReader(new InputStreamReader(proc.getInputStream(), "GBK"));
                String [] line1 = {"0","0","0","0","0","0","0","0"};
                int i = 0;
                while ((line1[i] = in1.readLine()) != null) {
                    //System.out.println(line1[i]);
                    i++;

                }
                //System.out.println(123456);
                System.out.println(line1[0]);

                if(line1[0].equals("diferent")) {
                    OutputStream out1 = new FileOutputStream(imgFilePath1);
                    out1.write(b);
                    out1.flush();
                    out1.close();
                }
                in1.close();
                proc.waitFor();

            } catch (IOException e) {
                e.printStackTrace();
            }catch (InterruptedException e) {
                e.printStackTrace();
            }


            String imgurl = "http://xxxxxxxx/" + tempFileName;
            //imageService.save(imgurl);

            List<String> list =new LinkedList<>();
            //执行分析图片
            Process proc2;
            try {
                //proc2 = Runtime.getRuntime().exec("D:\\CodeSoft\\Anaconda\\envs\\tf-gpu\\python C:\\Users\\Administrator\\PycharmProjects\\Agricultural-Disease-Classification-master\\resnet50\\resnettest.py");// 执行py文件
                //用输入输出流来截取结果
                String Pyloc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
                Pyloc = Pyloc.substring(1) + "Python/Disease/resnettest.py";
                proc2 = Runtime.getRuntime().exec("E:\\Anaconda\\envs\\tf-gpu\\python "+Pyloc);
                /*为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞*/
                Thread t=new Thread(new InputStreamRunnable(proc2.getErrorStream(),"ErrorStream"));
                t.start();
                BufferedReader in2 = new BufferedReader(new InputStreamReader(proc2.getInputStream(), "GBK"));
                String [] line = {"0","0","0","0","0","0"};
                int i = 0;
                while ((line[i] = in2.readLine()) != null) {

                    i++;

                }
                System.out.println(line[1]);
                //System.out.println(line[2]);

                try{
                    FileReader fileReader =new FileReader("E:/IDEA_CODE/herbtest/src/main/resources/static/img/PiePhoto/probability.txt");
                    BufferedReader bufferedReader =new BufferedReader(fileReader);

                    String str=null;
                    while((str=bufferedReader.readLine())!=null) {
                        if (str.trim().length() > 2) {
                            list.add(str);

                        }
                    }

                }catch (IOException e){
                    e.printStackTrace();
                }

                map1.put("DiseaseType",line[1]);
                map1.put("AnalysisProbabilityPath",list);
                //map1.put("AnalysisPhotoPath","http://localhost:8443/"+line[2]);
                in2.close();
                proc2.waitFor();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
        String s = "D:/HerbTrain/AgriculturalDisease_trainA/images/" + tempFileName;
        File file = new File(s);
        if(file.exists()) {
            file.delete();
        }
        return map1;
    }

    @RequestMapping(value = "/api/DiseasePreventionSuggest", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> Analysis(HttpServletRequest request, HttpServletResponse response,@RequestBody JSONObject params)throws UnsupportedEncodingException {
        //执行图片
        Map<String,Object> map2 = new HashMap<String,Object>();
        Process proc;
        String Disease = params.getString("Disease");
        //System.out.println(Disease);
        String Area = params.getString("Area");
        //System.out.println(Area);
        String Year = params.getString("Year");
        //System.out.println(Year);
        String Time = params.getString("Time");
        //System.out.println(Time);

        try {
            //System.out.println(Disease);
            //proc = Runtime.getRuntime().exec("D:\\CodeSoft\\Anaconda\\envs\\tf-gpu\\python C:\\Users\\Administrator\\PycharmProjects\\HerbSuggest\\HerbSuggest\\HerbSuggest.py "+Disease+" "+Area+" "+Year+" "+Time );// 执行py文件
            //用输入输出流来截取结果
            String Pyloc = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
            Pyloc = Pyloc.substring(1) + "Python/Prevention/HerbSuggest.py";
            proc = Runtime.getRuntime().exec("E:\\Anaconda\\envs\\tf-gpu\\python"+" "+Pyloc+" "+Disease+" "+Area+" "+Year+" "+Time );
            /*为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞*/
            //Thread t=new Thread(new InputStreamRunnable(proc.getErrorStream(),"ErrorStream"));
            //t.start();
            BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream(), "GBK"));

            String [] line = {"0","0","0","0","0","0"};
            int i = 0;

            while ((line[i] = in.readLine()) != null) {
                //System.out.println(line[i]);
                i++;

            }

            System.out.println(line[0]);

            map2.put("DiseasePreventionSuggest",line[0]);
            in.close();
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return map2;
    }
}
