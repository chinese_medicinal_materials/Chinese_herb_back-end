package com.herb.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.herb.domain.*;
import com.herb.service.HerbService;
import com.herb.utils.ExcelUtil;
import com.herb.utils.LocalFileUtil;
import com.herb.utils.result;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import static com.herb.common.httpRequestUtils.httpRequest;

@Controller
public class HerbController {

    @Autowired
    private HerbService herbservice;



    /**
     * 查询
     * @param params（查询的关键字）
     * @return 查询到的中药列表
     */
    @CrossOrigin
    @PostMapping(value = "api/herbsearch")
    @ResponseBody
    public Map<String,Object>  searchHerb(@RequestBody JSONObject params) {
        Subject subject = SecurityUtils.getSubject();
        System.out.println(111111);
        System.out.println(subject.getSession().getId());
        System.out.println(111111);
        String keyword = params.getString("keyword");
        System.out.println(keyword);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("herblist", herbservice.SearchHerb(keyword));
        System.out.println(JSON.toJSONString(map));
        return map;
    }

    /*@CrossOrigin
    @PostMapping(value = "api/herbsearch")
    @ResponseBody
    public List<Herb> searchHerb(@RequestBody JSONObject params) {
        System.out.println(111111);
        String keyword = params.getString("keyword");
        System.out.println(keyword);
        Map<String, Object> map = new HashMap<String, Object>();
        return herbservice.SearchHerb(keyword);
    }*/

    /**
     * 新增药材数据
     * @param herb herb
     * @return 返回操作状态码
     */
    @CrossOrigin
    @PostMapping(value = "api/herbsave")
    @ResponseBody
    public result  saveHerb(@RequestBody Herb herb) {
        System.out.println(111111);
        herbservice.SaveHerb(herb);
        return new result(200);
    }

    /**
     * 修改药材数据
     * @param herb
     * @return 返回操作状态码
     */
    @CrossOrigin
    @PostMapping(value = "api/herbupdate")
    @ResponseBody
    public result  updateHerb(@RequestBody Herb herb) {
        System.out.println(222222);
        herbservice.UpdateHerb(herb);
        return new result(200);
    }

    /**
     * 修改药材数据
     * @param params（要删除的中药材的ID号）
     * @return 返回操作状态码
     */
    @CrossOrigin
    @PostMapping(value = "api/herbdelete")
    @ResponseBody
    public result  deleteHerb(@RequestBody JSONObject params) {
        Long herbid = params.getLong("herbId");
        System.out.println(herbid);
        herbservice.DeleteHerb(herbid);
        return new result(200);
    }

    @PostMapping(value = "api/herb_map_info")
    @ResponseBody
    public List<Map<String,Object>>  HerbMapInfo() {
        Map<String,Object> map1 =new HashMap<String,Object>();
        Map<String,Object> map2 =new HashMap<String,Object>();
        Map<String,Object> map3 =new HashMap<String,Object>();
        Map<String,Object> map4 =new HashMap<String,Object>();
        Map<String,Object> map5 =new HashMap<String,Object>();
        Map<String,Object> map6 =new HashMap<String,Object>();
        Map<String,Object> map7 =new HashMap<String,Object>();
        Map<String,Object> map8 =new HashMap<String,Object>();
        Map<String,Object> map9 =new HashMap<String,Object>();
        Map<String,Object> map10 =new HashMap<String,Object>();
        Map<String,Object> map11 =new HashMap<String,Object>();
        Map<String,Object> map12 =new HashMap<String,Object>();
        Map<String,Object> map13 =new HashMap<String,Object>();
        Map<String,Object> map14 =new HashMap<String,Object>();
        Map<String,Object> map15 =new HashMap<String,Object>();
        Map<String,Object> map16 =new HashMap<String,Object>();
        Map<String,Object> map17 =new HashMap<String,Object>();
        Map<String,Object> map18 =new HashMap<String,Object>();
        Map<String,Object> map19 =new HashMap<String,Object>();
        Map<String,Object> map20 =new HashMap<String,Object>();
        Map<String,Object> map21 =new HashMap<String,Object>();
        Map<String,Object> map22 =new HashMap<String,Object>();
        Map<String,Object> map23 =new HashMap<String,Object>();
        Map<String,Object> map24 =new HashMap<String,Object>();
        Map<String,Object> map25 =new HashMap<String,Object>();
        Map<String,Object> map26 =new HashMap<String,Object>();
        Map<String,Object> map27 =new HashMap<String,Object>();
        Map<String,Object> map28 =new HashMap<String,Object>();
        Map<String,Object> map29 =new HashMap<String,Object>();
        Map<String,Object> map30 =new HashMap<String,Object>();
        Map<String,Object> map31 =new HashMap<String,Object>();

        map1.put("provinceName","黑龙江");
        map1.put("herbNumber",herbservice.CountHerbNumberByCityName("黑龙江"));
        map1.put("majorProductionAreas","吴孙县 逊克县 大同县 铁力市 通河县");
        map1.put("majorHerb","仓术 升麻 赤芍 白鲜皮 防风");

        map2.put("provinceName","吉林");
        map2.put("herbNumber",herbservice.CountHerbNumberByCityName("吉林"));
        map2.put("majorProductionAreas","安图县 桦甸市 抚松县 蛟河市 集安市");
        map2.put("majorHerb","穿山龙 升麻 两头尖 白鲜皮 防风");

        map3.put("provinceName","辽宁");
        map3.put("herbNumber",herbservice.CountHerbNumberByCityName("辽宁"));
        map3.put("majorProductionAreas","桓仁满族自治县 清原满族自治县 建昌县 新宾满族自治县 岫岩满族自治县");
        map3.put("majorHerb","苍术 红参 细辛 白鲜皮 ");

        map4.put("provinceName","天津");
        map4.put("herbNumber",herbservice.CountHerbNumberByCityName("天津"));
        map4.put("majorProductionAreas","和平区");
        map4.put("majorHerb","海螵蛸");

        map5.put("provinceName","河北");
        map5.put("herbNumber",herbservice.CountHerbNumberByCityName("河北"));
        map5.put("majorProductionAreas","安国市 围场满族蒙古族自治县 巨鹿县 丰宁满族自治县 内丘县");
        map5.put("majorHerb","苍术 鸡冠花 苦杏仁 南沙参 山楂");

        map6.put("provinceName","山西");
        map6.put("herbNumber",herbservice.CountHerbNumberByCityName("山西"));
        map6.put("majorProductionAreas","闻喜县 新绛县 绛县 襄汾县 潞城市");
        map6.put("majorHerb","黄芪 连翘 党参 丹参 大黄");

        map7.put("provinceName","山东");
        map7.put("herbNumber",herbservice.CountHerbNumberByCityName("山东"));
        map7.put("majorProductionAreas","莒县 平邑县 沂水县 郯县 微山县");
        map7.put("majorHerb","阿胶 柏子仁 干姜 槐角 芡实");

        map8.put("provinceName","河南");
        map8.put("herbNumber",herbservice.CountHerbNumberByCityName("河南"));
        map8.put("majorProductionAreas","莒县 平邑县 沂水县 郯县 微山县");
        map8.put("majorHerb","白芷 冬瓜子 预知子 丹参 连翘 ");

        map9.put("provinceName","陕西");
        map9.put("herbNumber",herbservice.CountHerbNumberByCityName("陕西"));
        map9.put("majorProductionAreas","城固县 祚水县 澄城县 商州区 陈仓区");
        map9.put("majorHerb","葛根 穿山龙 柴胡 连翘 木瓜");

        map10.put("provinceName","青海");
        map10.put("herbNumber",herbservice.CountHerbNumberByCityName("青海"));
        map10.put("majorProductionAreas","湟中县 河南蒙古族自治县 格尔木市 都兰市 大通回族土族自治县");
        map10.put("majorHerb","独一味 枸杞子 羌活 麻黄 甘草");

        map11.put("provinceName","上海");
        map11.put("herbNumber",herbservice.CountHerbNumberByCityName("上海"));
        map11.put("majorProductionAreas","暂无数据");
        map11.put("majorHerb","暂无数据");

        map12.put("provinceName","江苏");
        map12.put("herbNumber",herbservice.CountHerbNumberByCityName("江苏"));
        map12.put("majorProductionAreas","射阳市 邳州市 睢宁市 如皋市 泗阳县");
        map12.put("majorHerb","蛇床子 楮实子 瓜萎皮 蚕沙 薄荷");

        map13.put("provinceName","安徽");
        map13.put("herbNumber",herbservice.CountHerbNumberByCityName("安徽"));
        map13.put("majorProductionAreas","谯城区 金寨县 岳西县 宁国市 太和县");
        map13.put("majorHerb","路路通 木瓜 丝瓜络 楮实子 断血流");

        map14.put("provinceName","湖北");
        map14.put("herbNumber",herbservice.CountHerbNumberByCityName("湖北"));
        map14.put("majorProductionAreas","利川市 英山县 五峰土家族自治县 罗田县 夷陵区");
        map14.put("majorHerb","路路通 湖北贝母 五倍子 百部 白前 ");

        map15.put("provinceName","江西");
        map15.put("herbNumber",herbservice.CountHerbNumberByCityName("江西"));
        map15.put("majorProductionAreas","太和县 樟树市 信丰县 丰城市 龙南县");
        map15.put("majorHerb","防己 钩藤 青皮 白花蛇舌草 半边莲");

        map16.put("provinceName","浙江");
        map16.put("herbNumber",herbservice.CountHerbNumberByCityName("浙江"));
        map16.put("majorProductionAreas","磐安县 淳安县 柯城区 衢江区 新昌县");
        map16.put("majorHerb","三棱 白术 橘核 青皮 玉竹");

        map17.put("provinceName","湖南");
        map17.put("herbNumber",herbservice.CountHerbNumberByCityName("湖南"));
        map17.put("majorProductionAreas","隆回县 邵东县 靖州苗族侗族自治县 沅江县 凤凰县");
        map17.put("majorHerb","土茯苓 半边莲  百合 淡竹叶 葛花");

        map18.put("provinceName","福建");
        map18.put("herbNumber",herbservice.CountHerbNumberByCityName("福建"));
        map18.put("majorProductionAreas","仙游县 洛江区 龙海市 涵江区 古田县");
        map18.put("majorHerb","太子参 石决明 麦冬 路路通 降香");

        map19.put("provinceName","广东");
        map19.put("herbNumber",herbservice.CountHerbNumberByCityName("广东"));
        map19.put("majorProductionAreas","英德市 遂溪市 阳春市 德庆县 高州市");
        map19.put("majorHerb","穿心莲 巴戟天 广藿香 广金钱草 何首乌");

        map20.put("provinceName","海南");
        map20.put("herbNumber",herbservice.CountHerbNumberByCityName("海南"));
        map20.put("majorProductionAreas","琼山区 文昌市 琼中黎族苗族自治县");
        map20.put("majorHerb","益智 香附 砂仁 胡椒 地龙");

        map21.put("provinceName","广西");
        map21.put("herbNumber",herbservice.CountHerbNumberByCityName("广西"));
        map21.put("majorProductionAreas","暂无数据");
        map21.put("majorHerb","暂无数据");

        map22.put("provinceName","重庆");
        map22.put("herbNumber",herbservice.CountHerbNumberByCityName("重庆市"));
        map22.put("majorProductionAreas","垫江区  奉节县 石柱土家族自治县 长寿区 江津市");
        map22.put("majorHerb","黄柏 紫苏叶 大黄 川牛膝 半夏");

        map23.put("provinceName","贵州");
        map23.put("herbNumber",herbservice.CountHerbNumberByCityName("贵州"));
        map23.put("majorProductionAreas","独山县 安龙县 兴义市 西秀区 都匀市");
        map23.put("majorHerb","南沙参 路路通 钩藤 黄精 桑白皮");

        map24.put("provinceName","四川");
        map24.put("herbNumber",herbservice.CountHerbNumberByCityName("四川"));
        map24.put("majorProductionAreas","旺苍县 双流县 苍溪县 通江县 北川羌族自治县");
        map24.put("majorHerb","何首乌 木瓜 川贝母 川明参 甘松");

        map25.put("provinceName","云南");
        map25.put("herbNumber",herbservice.CountHerbNumberByCityName("云南"));
        map25.put("majorProductionAreas","砚山县 陆良县 洱源县 永胜县 大姚县");
        map25.put("majorHerb","木香 草果 红花 木瓜 秦艽");

        map26.put("provinceName","西藏");
        map26.put("herbNumber",herbservice.CountHerbNumberByCityName("西藏"));
        map26.put("majorProductionAreas","暂无数据");
        map26.put("majorHerb","暂无数据");

        map27.put("provinceName","新疆");
        map27.put("herbNumber",herbservice.CountHerbNumberByCityName("新疆"));
        map27.put("majorProductionAreas","暂无数据");
        map27.put("majorHerb","暂无数据");

        map28.put("provinceName","内蒙古");
        map28.put("herbNumber",herbservice.CountHerbNumberByCityName("内蒙古"));
        map28.put("majorProductionAreas","暂无数据");
        map28.put("majorHerb","暂无数据");

        map29.put("provinceName","宁夏");
        map29.put("herbNumber",herbservice.CountHerbNumberByCityName("宁夏"));
        map29.put("majorProductionAreas","暂无数据");
        map29.put("majorHerb","暂无数据");

        map30.put("provinceName","甘肃");
        map30.put("herbNumber",herbservice.CountHerbNumberByCityName("甘肃"));
        map30.put("majorProductionAreas","陇西县 岷县 宕昌县 民乐县 渭源县");
        map30.put("majorHerb","南沙参 大黄 黄芪 牛蒡子 党参");

        map31.put("provinceName","北京");
        map31.put("herbNumber",herbservice.CountHerbNumberByCityName("甘肃"));
        map31.put("majorProductionAreas","暂无数据");
        map31.put("majorHerb","暂无数据");

        List<Map<String,Object>> HerbMapInfo=new ArrayList<Map<String,Object>>();
        HerbMapInfo.add(map1);
        HerbMapInfo.add(map2);
        HerbMapInfo.add(map3);
        HerbMapInfo.add(map4);
        HerbMapInfo.add(map5);
        HerbMapInfo.add(map6);
        HerbMapInfo.add(map7);
        HerbMapInfo.add(map8);
        HerbMapInfo.add(map9);
        HerbMapInfo.add(map10);
        HerbMapInfo.add(map11);
        HerbMapInfo.add(map12);
        HerbMapInfo.add(map13);
        HerbMapInfo.add(map14);
        HerbMapInfo.add(map15);
        HerbMapInfo.add(map16);
        HerbMapInfo.add(map17);
        HerbMapInfo.add(map18);
        HerbMapInfo.add(map19);
        HerbMapInfo.add(map20);
        HerbMapInfo.add(map21);
        HerbMapInfo.add(map22);
        HerbMapInfo.add(map23);
        HerbMapInfo.add(map24);
        HerbMapInfo.add(map25);
        HerbMapInfo.add(map26);
        HerbMapInfo.add(map27);
        HerbMapInfo.add(map28);
        HerbMapInfo.add(map29);
        HerbMapInfo.add(map30);
        HerbMapInfo.add(map31);
        return HerbMapInfo;
    }

    @PostMapping(value = "api/getHerbYieldLocation")
    @ResponseBody
    public JSONArray getHerbYieldLocation(@RequestBody JSONObject params) {
        String herbName = params.getString("herbName");
        System.out.println("药材名是："+herbName);
        List<Herb> herblist = herbservice.SearchIdbyHerbname(herbName);
        JSONArray array = new JSONArray();
        for (Herb herb : herblist) {
            System.out.println(herb.getHerbId());
            System.out.println(JSON.toJSON(herbservice.getLocationByHerbId(herb.getHerbId())));
            array = JSONArray.parseArray(JSON.toJSONString(herbservice.getLocationByHerbId(herb.getHerbId())));
        }
        return array;
    }


    @PostMapping(value = "api/getYieldInfo")
    @ResponseBody
    public List<HerbYield> getHerbYieldByHerbNameAndLocation(@RequestBody JSONObject params){
        String herbName = params.getString("herbName");
        String Location = params.getString("location");
        List<Herb> herblist = herbservice.SearchIdbyHerbname(herbName);
        Long herbId = 0L;
        for (Herb herb : herblist) {
            herbId = herb.getHerbId();
        }
        return herbservice.getHerbYieldByHerbNameAndLocation(herbId,Location);
    }

    @PostMapping(value = "/api/suitableAreaDiscovery")
    @ResponseBody
    public Map<String,Object> SuitableAreaDiscovery(@RequestBody JSONObject params){
        String herbName = params.getString("herbName");
        return herbservice.SuitableAreaDiscovery(herbName);
    }

    @PostMapping("/api/uploadDistribution")
    @ResponseBody
    public result uploadExcel(HttpServletRequest request,
                              @RequestParam(value = "file", required = false) MultipartFile file,
                              @RequestParam(value = "userId") Integer  userId) throws IOException {
        System.out.println("当前用户的Id为"+userId);
        File f = null;
        try {
            // MultipartFile 转  file
            f = LocalFileUtil.multipartFileToFile(file);
            List<ExcelDistribute> list = ExcelUtil.importExcel(f, ExcelDistribute.class, 1);
            for ( ExcelDistribute data : list){
                String herbname = data.getHerbName();
                Double longitude = data.getLongitude();
                Double latitude=data.getLatitude();
                System.out.println("经度:"+longitude+"  纬度:"+latitude);
                Ecosystem ecosystem = herbservice.findEcosystemByLonAndLat(longitude,latitude);
                System.out.println(JSON.toJSONString(ecosystem));
                HerbDistribute herbDistribute =new HerbDistribute();
                herbDistribute.setEcosystemId(ecosystem.getEcosystemId());
                herbDistribute.setLatitude(latitude);
                herbDistribute.setLongitude(longitude);
                herbDistribute.setAvgTempAnn(ecosystem.getAvgTempAnn());
                herbDistribute.setAvgTempDayRange(ecosystem.getAvgTempDayRange());
                herbDistribute.setIsothermality(ecosystem.getIsothermality());
                herbDistribute.setTempSeasonality(ecosystem.getTempSeasonality());
                herbDistribute.setMaxTempWarmestMonth(ecosystem.getMaxTempWarmestMonth());
                herbDistribute.setMinTempColdestMonth(ecosystem.getMinTempColdestMonth());
                herbDistribute.setTempAnnRange(ecosystem.getTempAnnRange());
                herbDistribute.setAvgTempWettestQuarter(ecosystem.getAvgTempWettestQuarter());
                herbDistribute.setAvgTempDriestQuarter(ecosystem.getAvgTempDriestQuarter());
                herbDistribute.setAvgTempWarmestQuarter(ecosystem.getAvgTempWarmestQuarter());
                herbDistribute.setAvgTempColdestQuarter(ecosystem.getAvgTempColdestQuarter());
                herbDistribute.setPrecAnn(ecosystem.getPrecAnn());
                herbDistribute.setPrecWettestMonth(ecosystem.getPrecWettestMonth());
                herbDistribute.setPrecDriestMonth(ecosystem.getPrecDriestMonth());
                herbDistribute.setPrecSeasonality(ecosystem.getPrecSeasonality());
                herbDistribute.setPrecWarmestQuarter(ecosystem.getPrecWarmestQuarter());
                herbDistribute.setPrecDriestQuarter(ecosystem.getPrecDriestQuarter());
                herbDistribute.setPrecWarmestQuarter(ecosystem.getPrecWarmestQuarter());
                herbDistribute.setPrecColdestQuarter(ecosystem.getPrecColdestQuarter());
                herbDistribute.setPrecWettestMonth(ecosystem.getPrecWettestMonth());
                herbDistribute.setPrecWettestQuarter(ecosystem.getPrecWettestQuarter());
                herbDistribute.setProvince(ecosystem.getProvince());
                herbDistribute.setCity(ecosystem.getCity());
                herbDistribute.setDistrict(ecosystem.getDistrict());
                herbDistribute.setUserid(userId);
                herbDistribute.setHerbname(herbname);
                System.out.println(JSON.toJSONString(herbDistribute));
                herbservice.InsertHerbDistribution(herbDistribute);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (f != null) {
                f.delete();
            }
        }

        List<HerbDistribute> distributeList=herbservice.getAllDistribution();
        try {
            String pathname = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
            //此步骤主要为了定位到static目录之下
            //定位到具体的文件目录处
            File Csvfile = new File(pathname+"Python/SuitableAreaDiscovery");
            if (!Csvfile.exists()) {
                Csvfile.mkdirs();
            }
            Appendable printWriter = new PrintWriter(Csvfile + "/herb_distribute.csv");
            CSVPrinter csvPrinter = CSVFormat.EXCEL.withHeader("herbname","longitude","latitude","avg_temp_ann",
                    "avg_temp_day_range","isothermality","temp_seasonality","max_temp_warmest_month", "min_temp_coldest_month",
                    "temp_ann_range","avg_temp_wettest_quarter","avg_temp_driest_quarter","avg_temp_warmest_quarter",
                    "avg_temp_coldest_quarter","prec_ann","prec_wettest_month","prec_driest_month","prec_seasonality"
                    ,"prec_wettest_quarter","prec_driest_quarter","prec_warmest_quarter","prec_coldest_quarter")
                    .print(printWriter);
            for (HerbDistribute herbDistributeitem : distributeList) {
                csvPrinter.printRecord(herbDistributeitem.getHerbname(),herbDistributeitem.getLongitude(),
                        herbDistributeitem.getLatitude(),herbDistributeitem.getAvgTempAnn(),herbDistributeitem.getAvgTempDayRange(),
                        herbDistributeitem.getIsothermality(),herbDistributeitem.getTempSeasonality(),herbDistributeitem.getMaxTempWarmestMonth(),
                        herbDistributeitem.getMinTempColdestMonth(),herbDistributeitem.getTempAnnRange(),herbDistributeitem.getAvgTempWettestQuarter(),
                        herbDistributeitem.getAvgTempDriestQuarter(),herbDistributeitem.getAvgTempWarmestQuarter(),herbDistributeitem.getAvgTempColdestQuarter(),
                        herbDistributeitem.getPrecAnn(),herbDistributeitem.getPrecWettestMonth(),herbDistributeitem.getPrecDriestMonth(),
                        herbDistributeitem.getPrecSeasonality(),herbDistributeitem.getPrecWettestQuarter(),herbDistributeitem.getPrecDriestQuarter(),
                        herbDistributeitem.getPrecWarmestQuarter(),herbDistributeitem.getPrecColdestQuarter());
            }
            csvPrinter.flush();
            csvPrinter.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        return new result(200,"上传成功");
    }


    @PostMapping("/api/addDistribution")
    @ResponseBody
    public result addDistribution(HttpServletRequest request, @RequestBody JSONObject params) {
        Double longitude=params.getDouble("longitude");
        Double latitude =params.getDouble("latitude");
        String herbname=params.getString("herbname");
        Integer userId =params.getInteger("userId");
        System.out.println(latitude);
        System.out.println(longitude);
        System.out.println(herbname);
        System.out.println(userId);
        Ecosystem ecosystem = herbservice.findEcosystemByLonAndLat(longitude,latitude);
        HerbDistribute herbDistribute =new HerbDistribute();

        herbDistribute.setLatitude(latitude);
        herbDistribute.setLongitude(longitude);
        herbDistribute.setUserid(userId);
        herbDistribute.setHerbname(herbname);

        herbDistribute.setEcosystemId(ecosystem.getEcosystemId());
        herbDistribute.setAvgTempAnn(ecosystem.getAvgTempAnn());
        herbDistribute.setAvgTempDayRange(ecosystem.getAvgTempDayRange());
        herbDistribute.setIsothermality(ecosystem.getIsothermality());
        herbDistribute.setTempSeasonality(ecosystem.getTempSeasonality());
        herbDistribute.setMaxTempWarmestMonth(ecosystem.getMaxTempWarmestMonth());
        herbDistribute.setMinTempColdestMonth(ecosystem.getMinTempColdestMonth());
        herbDistribute.setTempAnnRange(ecosystem.getTempAnnRange());
        herbDistribute.setAvgTempWettestQuarter(ecosystem.getAvgTempWettestQuarter());
        herbDistribute.setAvgTempDriestQuarter(ecosystem.getAvgTempDriestQuarter());
        herbDistribute.setAvgTempWarmestQuarter(ecosystem.getAvgTempWarmestQuarter());
        herbDistribute.setAvgTempColdestQuarter(ecosystem.getAvgTempColdestQuarter());
        herbDistribute.setPrecAnn(ecosystem.getPrecAnn());
        herbDistribute.setPrecWettestMonth(ecosystem.getPrecWettestMonth());
        herbDistribute.setPrecDriestMonth(ecosystem.getPrecDriestMonth());
        herbDistribute.setPrecSeasonality(ecosystem.getPrecSeasonality());
        herbDistribute.setPrecWarmestQuarter(ecosystem.getPrecWarmestQuarter());
        herbDistribute.setPrecDriestQuarter(ecosystem.getPrecDriestQuarter());
        herbDistribute.setPrecWarmestQuarter(ecosystem.getPrecWarmestQuarter());
        herbDistribute.setPrecColdestQuarter(ecosystem.getPrecColdestQuarter());
        herbDistribute.setPrecWettestMonth(ecosystem.getPrecWettestMonth());
        herbDistribute.setPrecWettestQuarter(ecosystem.getPrecWettestQuarter());
        herbDistribute.setProvince(ecosystem.getProvince());
        herbDistribute.setCity(ecosystem.getCity());
        herbDistribute.setDistrict(ecosystem.getDistrict());

        herbservice.InsertHerbDistribution(herbDistribute);
        List<HerbDistribute> distributeList=herbservice.getAllDistribution();
        try {
            String pathname = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
            //此步骤主要为了定位到static目录之下
            File file = new File(pathname+"Python/SuitableAreaDiscovery");
            //定位到具体的文件目录处
            if (!file.exists()) {
                file.mkdirs();
            }
            Appendable printWriter = new PrintWriter(file + "/herb_distribute.csv");
            CSVPrinter csvPrinter = CSVFormat.EXCEL.withHeader("herbname","longitude","latitude","avg_temp_ann",
            "avg_temp_day_range","isothermality","temp_seasonality","max_temp_warmest_month", "min_temp_coldest_month",
            "temp_ann_range","avg_temp_wettest_quarter","avg_temp_driest_quarter","avg_temp_warmest_quarter",
            "avg_temp_coldest_quarter","prec_ann","prec_wettest_month","prec_driest_month","prec_seasonality"
            ,"prec_wettest_quarter","prec_driest_quarter","prec_warmest_quarter","prec_coldest_quarter")
                    .print(printWriter);
            for (HerbDistribute herbDistributeitem : distributeList) {
                csvPrinter.printRecord(herbDistributeitem.getHerbname(),herbDistributeitem.getLongitude(),
                herbDistributeitem.getLatitude(),herbDistributeitem.getAvgTempAnn(),herbDistributeitem.getAvgTempDayRange(),
                herbDistributeitem.getIsothermality(),herbDistributeitem.getTempSeasonality(),herbDistributeitem.getMaxTempWarmestMonth(),
                 herbDistributeitem.getMinTempColdestMonth(),herbDistributeitem.getTempAnnRange(),herbDistributeitem.getAvgTempWettestQuarter(),
                herbDistributeitem.getAvgTempDriestQuarter(),herbDistributeitem.getAvgTempWarmestQuarter(),herbDistributeitem.getAvgTempColdestQuarter(),
                herbDistributeitem.getPrecAnn(),herbDistributeitem.getPrecWettestMonth(),herbDistributeitem.getPrecDriestMonth(),
                herbDistributeitem.getPrecSeasonality(),herbDistributeitem.getPrecWettestQuarter(),herbDistributeitem.getPrecDriestQuarter(),
                herbDistributeitem.getPrecWarmestQuarter(),herbDistributeitem.getPrecColdestQuarter());
            }
            csvPrinter.flush();
            csvPrinter.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        System.out.println(herbservice.getAllDistribution());
        System.out.println(herbservice.getAllDistribution().toString());
        return new result(201,"修改成功");
    }

    @PostMapping("/api/findDistribution")
    @ResponseBody
    public Map<String,Object> findDistribution(HttpServletRequest request, @RequestBody JSONObject params) {
        Map<String, Object> map = new HashMap<String, Object>();
        JSONObject param = params.getJSONObject("params");
        String  query = param.getString("query");
        int userId = param.getIntValue("userId");
        int pagenum = param.getIntValue("pagenum");
        int pagesize = param.getIntValue("pagesize");
        System.out.println("页码"+pagenum);
        System.out.println("每页条数"+pagesize);
        List<HerbDistribute> Alllist= herbservice.findDistribution(query,userId);
        if(Alllist.size()<pagesize){
            pagenum = 1;
        }
        List<HerbDistribute> list= herbservice.findDistributionPage(query,userId,pagenum,pagesize);
        int total = Alllist.size();
        map.put("total",total);
        map.put("list",list);
        return map;
    }

    @PostMapping("/api/searchAllDistribution")
    @ResponseBody
    public Map<String,Object> searchAllDistribution(HttpServletRequest request, @RequestBody JSONObject params) {

        String  query = params.getString("herbName");

        Map<String, Object> map = new HashMap<String, Object>();

        List<String> cityList = new LinkedList<String >();

        Map<String, Object> locationMap = new LinkedHashMap<String, Object>();

        List<HerbDistribute> Alllist= herbservice.searchDistribution(query);


        for (HerbDistribute herbDistribute :Alllist){

            List<Double> long_lat = new LinkedList<>();
            long_lat.add( herbDistribute.getLongitude());
            long_lat.add( herbDistribute.getLatitude());
            System.out.println(herbDistribute.getLongitude());
            locationMap.put(herbDistribute.getProvince()+herbDistribute.getDistributionId(),long_lat);
            cityList.add(herbDistribute.getProvince()+herbDistribute.getDistributionId());
        }
        map.put("LocationMap",locationMap);
        map.put("cityList",cityList);
        return map;
    }

    @PostMapping("/api/statisticsDistribution")
    @ResponseBody
    public Map<String,Object> statisticsDistribution(HttpServletRequest request, @RequestBody JSONObject params) {
        String herbName = params.getString("herbName");
        List<HerbDistribute> list= herbservice.findDistributionByHerbName(herbName);
        Map<String, Object> map = new HashMap<String, Object>();
        int East_China_area = 0;
        int South_China_area = 0;
        int central_China_area = 0;
        int north_China_area = 0;
        int northwest_China_area = 0;
        int southwest_China_area = 0;
        int northeast_China_area = 0;
        List<String> Area = new LinkedList<String>();
        List<Integer> quantity= new LinkedList<Integer>();

        for(HerbDistribute data:list){
            //从记录之中获取得到经纬度
            double longitude =data.getLongitude();
            double latitude  = data.getLatitude();
            System.out.println(longitude);
            System.out.println(latitude);
            List<String> getPandC=new ArrayList<String>();
            String str = data.getProvince();
            System.out.println(str);
            //华东地区:山东、江苏、安徽、浙江、福建、江西、上海
            //华南地区：广东、广西、海南
            //华中地区：湖南、湖北、河南
            //华北地区：北京、天津、河北、山西、内蒙古
            //西北地区：宁夏、新疆、青海、陕西、甘肃
            //西南地区：四川、云南、贵州、西藏、重庆
            //东北地区：辽宁、吉林、黑龙江省
            if (str.equals("山东省")  | str.equals("江苏省") | str.equals("安徽省")
                    | str.equals("浙江省") | str.equals("福建省") | str.equals("江西省") | str.equals("上海市") ){
                East_China_area++;
            }
            else if (str.equals("广东省") | str.equals("广西壮族自治区") | str.equals("海南省") ){
                South_China_area++;
            }
            else if ( str.equals("湖南省") | str.equals("湖北省") | str.equals("河南省") ){
                central_China_area++;
            }
            else if (str.equals("四川省") | str.equals("云南省") | str.equals("贵州省") | str.equals("西藏自治区") | str.equals("重庆市")){
                north_China_area++;
            }
            else if (str.equals("宁夏回族自治区") | str.equals("新疆维吾尔自治区") | str.equals("青海省") | str.equals("陕西省") | str.equals("甘肃省") ){
                northwest_China_area ++;
            }
            else if (str.equals("北京市") | str.equals("天津市") | str.equals("河北省") | str.equals("山西省" ) | str.equals("内蒙古自治区") ){
                southwest_China_area++;
            }
            else if (str.equals("吉林省")  | str.equals("辽宁省")  | str .equals("黑龙江省") ){
                northeast_China_area++;
            }

        }
        Area.add("华东地区");
        Area.add("华南地区");
        Area.add("华中地区");
        Area.add("华北地区");
        Area.add("西北地区");
        Area.add("西南地区");
        Area.add("东北地区");
        quantity.add(East_China_area);
        quantity.add(South_China_area);
        quantity.add(central_China_area);
        quantity.add(north_China_area);
        quantity.add(northwest_China_area);
        quantity.add(southwest_China_area);
        quantity.add(northeast_China_area);
        map.put("Area",Area);
        map.put("quantity",quantity);
        return map;
    }

    @PostMapping("/api/getDistribution")
    @ResponseBody
    public Map<String,Object> getDistribution(HttpServletRequest request,@RequestBody JSONObject params){
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("参数为"+params.getLong("distributionId"));
        HerbDistribute herbDistribute =  herbservice.findDistributionByPK(params.getLong("distributionId"));
        map.put("herbDistribute",herbDistribute);
        return map;
    }

    @PostMapping("/api/UpdateDistribution")
    @ResponseBody
    public result UpdateDistribution(HttpServletRequest request, @RequestBody HerbDistribute params){
        Long distributionId = params.getDistributionId();
        System.out.println("参数为"+params.getAvgTempAnn());
        herbservice.UpdateDistribution(params);

        List<HerbDistribute> distributeList=herbservice.getAllDistribution();
        try {
            String pathname = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
            //此步骤主要为了定位到static目录之下
            //定位到具体的文件目录处
            File Csvfile = new File(pathname+"Python/SuitableAreaDiscovery");
            if (!Csvfile.exists()) {
                Csvfile.mkdirs();
            }
            Appendable printWriter = new PrintWriter(Csvfile + "/herb_distribute.csv");
            CSVPrinter csvPrinter = CSVFormat.EXCEL.withHeader("herbname","longitude","latitude","avg_temp_ann",
                    "avg_temp_day_range","isothermality","temp_seasonality","max_temp_warmest_month", "min_temp_coldest_month",
                    "temp_ann_range","avg_temp_wettest_quarter","avg_temp_driest_quarter","avg_temp_warmest_quarter",
                    "avg_temp_coldest_quarter","prec_ann","prec_wettest_month","prec_driest_month","prec_seasonality"
                    ,"prec_wettest_quarter","prec_driest_quarter","prec_warmest_quarter","prec_coldest_quarter")
                    .print(printWriter);
            for (HerbDistribute herbDistributeitem : distributeList) {
                csvPrinter.printRecord(herbDistributeitem.getHerbname(),herbDistributeitem.getLongitude(),
                        herbDistributeitem.getLatitude(),herbDistributeitem.getAvgTempAnn(),herbDistributeitem.getAvgTempDayRange(),
                        herbDistributeitem.getIsothermality(),herbDistributeitem.getTempSeasonality(),herbDistributeitem.getMaxTempWarmestMonth(),
                        herbDistributeitem.getMinTempColdestMonth(),herbDistributeitem.getTempAnnRange(),herbDistributeitem.getAvgTempWettestQuarter(),
                        herbDistributeitem.getAvgTempDriestQuarter(),herbDistributeitem.getAvgTempWarmestQuarter(),herbDistributeitem.getAvgTempColdestQuarter(),
                        herbDistributeitem.getPrecAnn(),herbDistributeitem.getPrecWettestMonth(),herbDistributeitem.getPrecDriestMonth(),
                        herbDistributeitem.getPrecSeasonality(),herbDistributeitem.getPrecWettestQuarter(),herbDistributeitem.getPrecDriestQuarter(),
                        herbDistributeitem.getPrecWarmestQuarter(),herbDistributeitem.getPrecColdestQuarter());
            }
            csvPrinter.flush();
            csvPrinter.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        return new result(201,"修改成功");
    }

    @PostMapping("/api/DeleteDistribution")
    @ResponseBody
    public result DeleteDistribution(HttpServletRequest request, @RequestBody JSONObject params){
        System.out.println("参数为"+params.getJSONArray("distributionIdList"));
        JSONArray distributionIdList =  params.getJSONArray("distributionIdList");
        for(Object js :distributionIdList){
            LinkedHashMap<String,Object> map = (LinkedHashMap<String,Object>)js;
            System.out.println(map.get("distributionId"));
            herbservice.deleteDistribution(Integer.valueOf((Integer)map.get("distributionId")).longValue());
        }

        List<HerbDistribute> distributeList=herbservice.getAllDistribution();
        try {
            String pathname = this.getClass().getResource("/").getPath().replace("target/classes/","src/main/resources/static/");
            //此步骤主要为了定位到static目录之下
            //定位到具体的文件目录处
            File Csvfile = new File(pathname+"Python/SuitableAreaDiscovery");
            if (!Csvfile.exists()) {
                Csvfile.mkdirs();
            }
            Appendable printWriter = new PrintWriter(Csvfile + "/herb_distribute.csv");
            CSVPrinter csvPrinter = CSVFormat.EXCEL.withHeader("herbname","longitude","latitude","avg_temp_ann",
                    "avg_temp_day_range","isothermality","temp_seasonality","max_temp_warmest_month", "min_temp_coldest_month",
                    "temp_ann_range","avg_temp_wettest_quarter","avg_temp_driest_quarter","avg_temp_warmest_quarter",
                    "avg_temp_coldest_quarter","prec_ann","prec_wettest_month","prec_driest_month","prec_seasonality"
                    ,"prec_wettest_quarter","prec_driest_quarter","prec_warmest_quarter","prec_coldest_quarter")
                    .print(printWriter);
            for (HerbDistribute herbDistributeitem : distributeList) {
                csvPrinter.printRecord(herbDistributeitem.getHerbname(),String.valueOf(herbDistributeitem.getLongitude()),
                        String.valueOf(herbDistributeitem.getLatitude()), String.valueOf(herbDistributeitem.getAvgTempAnn()), String.valueOf(herbDistributeitem.getAvgTempDayRange()),
                        String.valueOf(herbDistributeitem.getIsothermality()), String.valueOf(herbDistributeitem.getTempSeasonality()), String.valueOf(herbDistributeitem.getMaxTempWarmestMonth()),
                        String.valueOf(herbDistributeitem.getMinTempColdestMonth()), String.valueOf(herbDistributeitem.getTempAnnRange()), String.valueOf(herbDistributeitem.getAvgTempWettestQuarter()),
                        String.valueOf(herbDistributeitem.getAvgTempDriestQuarter()), String.valueOf(herbDistributeitem.getAvgTempWarmestQuarter()), String.valueOf(herbDistributeitem.getAvgTempColdestQuarter()),
                        String.valueOf(herbDistributeitem.getPrecAnn()), String.valueOf(herbDistributeitem.getPrecWettestMonth()), String.valueOf(herbDistributeitem.getPrecDriestMonth()),
                        String.valueOf(herbDistributeitem.getPrecSeasonality()), String.valueOf(herbDistributeitem.getPrecWettestQuarter()), String.valueOf(herbDistributeitem.getPrecDriestQuarter()),
                        String.valueOf(herbDistributeitem.getPrecWarmestQuarter()), String.valueOf(herbDistributeitem.getPrecColdestQuarter()));
            }
            csvPrinter.flush();
            csvPrinter.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        return new result(200,"修改成功");
    }

    @PostMapping("/api/ReverseGeocoding")
    @ResponseBody
    public List<Ecosystem> ReverseGeocoding(HttpServletRequest request){
        List<Ecosystem> list= herbservice.ReverseGeocoding();
        for(Ecosystem data:list){
            Double longitude = data.getLongitude();
            Double latitude=data.getLatitude();
            System.out.println("经度:"+longitude+"  纬度:"+latitude);
            String outputStr="location="+longitude+","+latitude+"&key=a5ac3ad4fc1f14c538ffd7782a83815a&radius=1000&extensions=all";
            String str=httpRequest("https://restapi.amap.com/v3/geocode/regeo", "GET", outputStr);
            System.out.println(str);
            JSONObject jsonObject =(JSONObject)JSON.parse(str);
            JSONObject regeocode  = (JSONObject)jsonObject.get("regeocode");
            JSONObject addressComponent  = (JSONObject)regeocode.get("addressComponent");
            String country = "";
            System.out.println("com.alibaba.fastjson.JSONArray"==addressComponent.get("country").getClass().getName());
            System.out.println("java.lang.String"==addressComponent.get("country").getClass().getName());
            if("com.alibaba.fastjson.JSONArray"==addressComponent.get("country").getClass().getName()){
                JSONArray JSONcountry  = (JSONArray)addressComponent.get("country");
                country = JSONcountry.toString();
            }
            else if("java.lang.String"==addressComponent.get("country").getClass().getName()){
                country  = (String)addressComponent.get("country");
            }
            //String  country  = (String)addressComponent.get("country");
            System.out.println("国家是:"+country);
            System.out.println("[]".equals( country));
            System.out.println("序号:"+data.getEcosystemId());
            if( "[]".equals(country) ){
                data.setProvince("");
                data.setCity("");
                data.setDistrict("");
            }else{
                int start=str.indexOf("addressComponent");
                int end=str.indexOf("aois");
                str=str.substring(start, end);
                System.out.println(str);
                start=str.indexOf("province");
                end=str.indexOf("adcode");
                String province;
                String city;
                String district;
                province = str.substring(start+11, end-3);
                start=str.indexOf("city");
                end=str.indexOf(",");
                city = str.substring(start+7, end-1);
                start=str.indexOf("district");
                end=str.indexOf("towncode");
                district = str.substring(start+11, end-3);
                System.out.println("省份:"+province);
                System.out.println("城市:"+city);
                System.out.println("区/县:"+district);
                data.setProvince(province);
                data.setCity(city);
                data.setDistrict(district);
            }
            herbservice.UpdateEcosystem(data);
        }
        return list;
    }

}