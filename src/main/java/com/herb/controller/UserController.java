package com.herb.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.herb.domain.Role;
import com.herb.domain.User;
import com.herb.domain.UserRole;
import com.herb.service.UserService;
import com.herb.utils.result;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionContext;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.session.mgt.DefaultWebSessionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;
import org.apache.shiro.SecurityUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.io.Serializable;
import java.util.*;


@RestController
public class UserController {
    @Autowired
    private UserService userService;

    //退出登录


    @GetMapping("api/need_login")
    public result logout() {
        System.out.println("执行了api/logout");
        return new result(400,"尚未登陆，请先登陆");
    }


//    @CrossOrigin
//    @ResponseBody
//    @PostMapping("api/login")
//    public result loginUser(@RequestParam("username") String username,
//                            @RequestParam("password") String password,
//                            Model model,
//                            HttpServletRequest request,
//                            HttpServletResponse response
//    ) {
//
//
//        //这里代码一般是固定
//        response.setContentType("text/html;charset=utf-8");
//        // 设置响应头允许ajax跨域访问
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        // 星号表示所有的异域请求都可以接受
//        response.setHeader("Access-Control-Allow-Methods", "GET,POST");
//
//        System.out.println(username);
//        System.out.println(password);
//
//        List<User> users = userService.getUserList();
//        for(User u : users) {
//            if(u.getUsername().equals(username)) {
//                if(u.getPassword().equals(password)) {
//                    System.out.println("登录成功");
//                    System.out.println(username+":"+password);
//                    return new result(200);
//
//                }
//            }
//        }
//        return new result(400);
//    }


    @CrossOrigin
    @PostMapping(value = "api/login")
    @ResponseBody
    public Map<String,Object> logintest(@RequestBody User requestUser) {
        // 对 html 标签进行转义，防止 XSS 攻击
        System.out.println(JSON.toJSONString(requestUser));
        String username = requestUser.getUsername();
        System.out.println(JSON.toJSONString(requestUser));
        Map<String, Object> map = new HashMap<String, Object>();

        // 1.获取Subject
        Subject subject = SecurityUtils.getSubject();

        // 2.封装用户数据
        String Email = requestUser.getEmail();
        String password = requestUser.getPassword();
        if(Email != null && Email.length()!=0){
            username = Email;
        }
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(true);
        try{
            subject.login(token);
            /*Collection<Session> sessions = sessionDAO.getActiveSessions();
            for (Session session:sessions){
                System.out.println("登陆用户"+session.getId());
            }*/
            if(subject.hasRole("admin")){
                Serializable tokenId = subject.getSession().getId();
                System.out.println(tokenId);
                map.put("SessionId",tokenId);
                map.put("role","admin");
                map.put("roleid","1");
                map.put("user",subject.getPrincipal());
            }
            if(subject.hasRole("user")){
                Serializable tokenId = subject.getSession().getId();
                System.out.println(tokenId);
                map.put("SessionId",tokenId);
                map.put("role","user");
                map.put("roleid","2");
                map.put("user",subject.getPrincipal());
            }
        } catch (UnknownAccountException e){
            e.printStackTrace();
            map.put("msg","用户名不存在！");
            map.put("code", 400);
            return map;
        } catch (IncorrectCredentialsException e){
            map.put("msg","密码错误！");
            map.put("code", 400);
            return map;
        }


        map.put("code", 200);
        return map;

    }
    /**
     * @author QiuJIntao
     * @date 2020/4/14
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/register")
    @ResponseBody
    public result register(@RequestBody User user) {

        System.out.println(JSON.toJSONString(user));
        //要求： 号码与邮箱至少一个不为空
        int status = userService.register(user);
        switch (status) {
            case 0:
                return new result(200,"注册成功");
            case 1:
                return new result(400,"用户名和密码不能为空");
            case 2:
                return new result(400,"邮箱和号码至少填一个");
            case 3:
                return new result(400,"请输入正确的邮箱格式");
            case 4:
                return new result(400,"请输入正确的大陆号码格式");
            case 5:
                return new result(400,"此号码已被注册");
            case 6:
                return new result(400,"此邮箱已被注册");
        }
        return new result(400,"未知错误");
    }

    @PostMapping(value = "api/updateUserInfo")
    @ResponseBody
    public User updateUserInfo(@RequestBody User User){
        User updatedUser = userService.updateUserInfo(User);
        return updatedUser;
    }


    @PostMapping(value = "api/changePassword")
    @ResponseBody
    public result changePassword(@RequestBody JSONObject params){
        int userid = params.getInteger("userid");
        String oldPwd = params.getString("oldPwd");
        String newPwd = params.getString("newPwd");
        int code =userService.changePassword(userid,oldPwd,newPwd);
        if(code == 1){
            return new result(333,"修改成功");
        }
        else{
            return new result(222,"原密码错误");
        }
    }

}

