package com.herb.shiro;

import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class AjaxPermissionsAuthorizationFilter extends FormAuthenticationFilter{

    public AjaxPermissionsAuthorizationFilter(){
        super();
    }

    /*重写认证被拒绝的方法*/
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        System.out.println(1111111);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",300);
        jsonObject.put("msg","温馨提示，没有权限请登陆");
        PrintWriter out = null;
        HttpServletResponse res = (HttpServletResponse) response;
        try {
            res.setCharacterEncoding("UTF-8");
            res.setContentType("application/json");
            out = response.getWriter();
            out.println(jsonObject);
        } catch (Exception e) {
        } finally {
            if (null != out) {
                out.flush();
                out.close();
            }
        }
        return false;
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject,
                                     ServletRequest request, ServletResponse response) throws Exception {
        // 获取登录信息
        String user = (String) subject.getPrincipal();
        System.out.println("登录账户:"+user);
        Session session = subject.getSession();
        session.setAttribute("msg", "自定义FormFilter传递的信息...");
        return super.onLoginSuccess(token, subject, request, response);
    }


    @Bean
    public FilterRegistrationBean registration(AjaxPermissionsAuthorizationFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);
        return registration;
    }



}
