package com.herb.shiro;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.herb.domain.Role;
import com.herb.domain.User;
import com.herb.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class UserRealm extends AuthorizingRealm {
    private Logger logger = LoggerFactory.getLogger(UserRealm.class);

    @Autowired
    private UserService userService;

    @Override
    /*
     *执行授权逻辑 用于授权
     */
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        System.out.println("执行授权逻辑");
        Subject subject = SecurityUtils.getSubject();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        User user = (User)subject.getPrincipal();

        if(user != null)
        {
            Collection<String> rolesCollection = new HashSet<>();
            List<Role> rolelist = userService.findRoles(user.getUserid());
            for(Role role : rolelist) {
                rolesCollection.add(role.getRolename());
            }
            info.addRoles(rolesCollection);
            return info;
        }

        return null;
        /*  Session session = SecurityUtils.getSubject().getSession();
        //查询用户的权限
        //为当前用户设置角色和权限
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        return authorizationInfo;*/
    }

    /*
     *执行认证逻辑 用于用户登陆
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo();
        System.out.println("===执行认证===");


        UsernamePasswordToken token = (UsernamePasswordToken)authcToken;

        System.out.println(token.getUsername());

        User user = userService.FindByPhoneOrEmail(token.getUsername());
        if(user == null){
            throw new UnknownAccountException();
        }

        // 加密方式
        String hashAlgorithName = "MD5";
        // 加密次数
        int hashIterations = 2;
        //当前realm对象的name
        String realmName = getName();
        ByteSource credentialsSalt = ByteSource.Util.bytes(user.getSalt());
        System.out.println(user.getSalt());
        System.out.println(credentialsSalt);

        return new SimpleAuthenticationInfo(user, user.getPassword(),credentialsSalt, realmName);

    }

}
