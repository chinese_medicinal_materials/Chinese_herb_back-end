package com.herb.shiro;


import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier("securityManager") SecurityManager securityManager)
    {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        LogoutFilter logoutFilter = new LogoutFilter();
        logoutFilter.setRedirectUrl("need_login");

        Map<String, Filter> filterMap = new LinkedHashMap<>();
        filterMap.put("authc", new AjaxPermissionsAuthorizationFilter());
        filterMap.put("roles", new MyPermsFilter());
        filterMap.put("logout", logoutFilter);
        shiroFilterFactoryBean.setFilters(filterMap);

        /*
         * 此部分为shiro内置过滤器介绍
         * anon：无需进行认证(登陆)即可访问
         * authc：必须进行认证(登陆)才可访问
         * user: 如果使用rememberMe功能可以直接访问
         * perms: 使该资源必须得到资源权限才可使用
         * role: 该资源必须得到角色权限才可以访问
         * */
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();

        filterChainDefinitionMap.put("/api/admin/content/article", "anon");
        filterChainDefinitionMap.put("/api/article/**", "anon");
        filterChainDefinitionMap.put("/api/login", "anon");
        filterChainDefinitionMap.put("/api/register", "anon");
        filterChainDefinitionMap.put("/api/downloadTemplateExcel", "anon");
        filterChainDefinitionMap.put("/api/getherbnamelist", "anon");
        filterChainDefinitionMap.put("/api/herb_map_info", "anon");
        filterChainDefinitionMap.put("/api/getHerbYieldLocation", "anon");
        filterChainDefinitionMap.put("/api/uploadDistribution", "anon");
        filterChainDefinitionMap.put("/api/getYieldInfo", "anon");
        filterChainDefinitionMap.put("/api/updateUserInfo", "anon");
        filterChainDefinitionMap.put("/api/changePassword", "anon");
        filterChainDefinitionMap.put("/api/suitableAreaDiscovery", "anon");
        filterChainDefinitionMap.put("/api/findDistribution", "anon");
        filterChainDefinitionMap.put("/api/statisticsDistribution", "anon");
        filterChainDefinitionMap.put("/api/UpdateDistribution", "anon");
        filterChainDefinitionMap.put("/api/DenleteDistributio", "anon");
        filterChainDefinitionMap.put("/api/ReverseGeocoding", "anon");
        filterChainDefinitionMap.put("/api/getDistribution", "anon");
        filterChainDefinitionMap.put("/api/addDistribution", "anon");
        filterChainDefinitionMap.put("/api/searchAllDistribution", "anon");
        //filterChainDefinitionMap.put("/api/herbsearch", "anon");
        filterChainDefinitionMap.put("/api/herbsearch", "roles");
        filterChainDefinitionMap.put("/api/logout", "logout");
        filterChainDefinitionMap.put("/api/uploadbase64", "anon");
        filterChainDefinitionMap.put("/**", "anon");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }

    @Bean(name="securityManager")
    public DefaultWebSecurityManager securityManager(@Qualifier("userRealm") UserRealm userRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        //自定义的shiro session 缓存管理器，如果不是前后端分离，则无需设置SessionManager
        securityManager.setSessionManager(sessionManager());

        //关联realm（有坑，最好放在最后不然在某些版本会不生效）
        securityManager.setRealm(userRealm);

        return securityManager;
    }

    /**
     * 自定义realm
     * @param matcher
     * @return
     */
    @Bean("userRealm")
    public UserRealm userRealm( @Qualifier("hashedCredentialsMatcher")
                                        HashedCredentialsMatcher matcher) {
        UserRealm userRealm = new UserRealm();
        userRealm.setAuthorizationCachingEnabled(false);
        userRealm.setCredentialsMatcher(matcher);
        return userRealm;
    }

    /**
     * 自定义的 shiro session 缓存管理器，用于跨域等情况下使用 token（Authorization）进行验证，不依赖于sessionId
     * @return
     */
    @Bean
    public SessionManager sessionManager(){
        //将我们继承后重写的shiro session 注册
        ShiroSession shiroSession = new ShiroSession();
        //设置超时时间，默认30分钟会话超时，方法之中的单位是毫秒
        shiroSession.setGlobalSessionTimeout(30*60*1000);
        //如果后续考虑多tomcat部署应用，可以使用shiro-redis开源插件来做session 的控制，或者nginx 的负载均衡
        shiroSession.setSessionDAO(new EnterpriseCacheSessionDAO());
        return shiroSession;
    }

    /**
     * 密码加解密规则
     * @return
     */
    @Bean("hashedCredentialsMatcher")
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        //指定加密方式为MD5
        credentialsMatcher.setHashAlgorithmName("MD5");
        //加密次数
        credentialsMatcher.setHashIterations(2);
        credentialsMatcher.setStoredCredentialsHexEncoded(true);
        return credentialsMatcher;
    }

}
